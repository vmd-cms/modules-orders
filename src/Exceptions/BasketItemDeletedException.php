<?php

namespace VmdCms\Modules\Orders\Exceptions;

use VmdCms\CoreCms\Exceptions\CoreException;

class BasketItemDeletedException extends CoreException
{

}
