<?php

namespace VmdCms\Modules\Orders\Controllers;

use App\Modules\Content\DTO\BlockDTO;
use App\Modules\Content\Services\DataShare;
use App\Modules\Orders\Entity\OrderEntity;
use App\Modules\Orders\Models\Components\Blocks\QuickOrderCompletedBlock;
use App\Modules\Orders\Models\Components\Blocks\QuickOrderFailedBlock;
use Illuminate\Http\Request;
use VmdCms\CoreCms\Controllers\CoreController;

class OrderController extends CoreController
{
    protected $logChannel = 'orders';

    public function quickBuy(Request $request)
    {
        $request->validate([
            'phone' => 'required',
            'price_id' => 'required|int'
        ]);

        $this->logger('Creating QuickOrder. Data: ' . json_encode([
                'price_id' => $request->get('price_id'),
                'phone' => $request->get('phone'),
                'auth_user_id' => auth()->guard('user')->check() ? auth()->guard('user')->id() : null
            ]));

        try {
            $orderId = (new OrderEntity())->quickBuyAction($request->get('price_id'), $request->get('phone'));
        }
        catch (\Exception $exception)
        {
            $this->logger($exception->getMessage(),'debug');
            DataShare::getInstance()->blocks->append(new BlockDTO(QuickOrderFailedBlock::getModelTree()));
            return view('content::layouts.pages.quick_order_failed')->render();
        }
        $this->logger('Created QuickOrder id:' . $orderId,'info');
        DataShare::getInstance()->blocks->append(new BlockDTO(QuickOrderCompletedBlock::getModelTree()));
        return view('content::layouts.pages.quick_order_complete')->render();
    }
}
