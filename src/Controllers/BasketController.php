<?php

namespace VmdCms\Modules\Orders\Controllers;

use App\Modules\Content\Services\DataShare;
use App\Modules\Orders\Entity\BasketEntity;
use App\Modules\Orders\Models\Components\OrderTranslate;
use Illuminate\Http\Request;
use VmdCms\CoreCms\Controllers\CoreController;

class BasketController extends CoreController
{
    public function appendToBasket(Request $request)
    {
        try {
            $request->validate([
                'priceId' => 'required|int'
            ]);
            $basketEntity = new BasketEntity();
            $basketEntity->appendToBasket($request->get('priceId'));

            $data = DataShare::getInstance(false);
            $data->basketInfo = $basketEntity->getBasketInfoDto();
            $data->translates->appendGroups([OrderTranslate::getModelGroup()]);
            $basketPreview = view('content::layouts.components.basket_preview',['data' => $data])->render();
        }
        catch (\Exception $exception)
        {
            return json_encode([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }

        return json_encode([
            'status' => true,
            'basketPreview' => $basketPreview,
            'countItems' => count($data->basketInfo->getBasketItems()),
        ]);
    }

    public function changeBasketItemCount(Request $request)
    {
        try {
            $request->validate([
                'price_id' => 'required|int',
                'quantity' => 'required|int|min:1',
            ]);
            $basketEntity = new BasketEntity();
            $basketItemDTO = $basketEntity->changeBasketItemCount($request->get('price_id'),$request->get('quantity'));
            $basketItemPriceBlock = view('content::layouts.components.basket_item_price_block',['basketItemDTO'=>$basketItemDTO])->render();
            $basketInfoDTO = $basketEntity->getBasketInfoDto();
            return json_encode([
                'status' => true,
                'quantity' => $basketItemDTO->getQuantity(),
                'priceBlock' => $basketItemPriceBlock,
                'totalQuantity' => $basketInfoDTO->getTotalQuantityStr(),
                'totalSum' => $basketInfoDTO->getTotalSumStr(),
            ]);
        }
        catch (\Exception $exception)
        {
            return json_encode([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function removeBasketItem(Request $request)
    {
        try {
            $request->validate([
                'price_id' => 'required|int'
            ]);
            $basketEntity = new BasketEntity();
            $basketEntity->removeBasketItem($request->get('price_id'));

            $data = DataShare::getInstance(false);
            $data->basketInfo = $basketEntity->getBasketInfoDto();
            $data->translates->appendGroups([OrderTranslate::getModelGroup()]);
            $basketPreview = view('content::layouts.components.basket_preview',['data' => $data])->render();
            return json_encode([
                'status' => true,
                'hasItems' => boolval($data->basketInfo->getTotalQuantity()),
                'totalQuantity' => $data->basketInfo->getTotalQuantityStr(),
                'totalSum' => $data->basketInfo->getTotalSumStr(),
                'basketPreview' => $basketPreview,
            ]);
        }
        catch (\Exception $exception)
        {
            return json_encode([
                'error' => true,
                'message' => $exception->getMessage()
            ]);
        }
    }

    public function basketPage(Request $request)
    {
        (new BasketEntity())->shareBasketPageData();
        return view('content::layouts.pages.basket')->render();
    }
}
