<?php

namespace VmdCms\Modules\Orders\Enums;

class OrderStatusEnum
{
    const NEW = 'new';
    const PAYMENT_WAITING = 'payment_waiting';
    const PAYMENT_FAILED = 'payment_failed';
    const PAYMENT_SUCCESS = 'payment_success';
    const DELIVERY_WAITING = 'delivery_waiting';
    const DELIVERY_SUCCESS = 'delivery_success';
    const COMPLETED = 'completed';

    public static function enums(){
        return [
            static::NEW => static::NEW,
            static::PAYMENT_WAITING => static::PAYMENT_WAITING,
            static::PAYMENT_FAILED => static::PAYMENT_FAILED,
            static::PAYMENT_SUCCESS => static::PAYMENT_SUCCESS,
            static::DELIVERY_WAITING => static::DELIVERY_WAITING,
            static::DELIVERY_SUCCESS => static::DELIVERY_SUCCESS,
            static::COMPLETED => static::COMPLETED,
        ];
    }
}
