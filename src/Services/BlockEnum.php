<?php

namespace VmdCms\Modules\Orders\Services;

use VmdCms\CoreCms\Services\Enums;

class BlockEnum extends Enums
{
    const ORDER_INFO_BLOCK = 'order_info_block';
}
