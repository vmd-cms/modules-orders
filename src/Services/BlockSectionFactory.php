<?php

namespace VmdCms\Modules\Orders\Services;

use VmdCms\CoreCms\Services\BlockSectionFactoryAbstract;
use VmdCms\Modules\Orders\Models\Components\Blocks\OrderInfoBlock;

class BlockSectionFactory extends BlockSectionFactoryAbstract
{
    protected static function getAssocKeySectionClasses() : array
    {
        return [
            OrderInfoBlock::getModelKey() => \App\Modules\Orders\Sections\Components\Blocks\OrderInfoBlock::class,
        ];
    }
}
