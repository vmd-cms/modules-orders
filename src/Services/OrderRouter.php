<?php

namespace VmdCms\Modules\Orders\Services;

class OrderRouter
{
    const BASKET_PREFIX = 'basket.';

    const ROUTE_APPEND_TO_BASKET = self::BASKET_PREFIX . 'append_to_basket';
    const ROUTE_CHANGE_BASKET_ITEM_COUNT = self::BASKET_PREFIX . 'change_item_count';
    const ROUTE_REMOVE_BASKET_ITEM = self::BASKET_PREFIX . 'remove_item';
    const ROUTE_BASKET = self::BASKET_PREFIX . 'basket';

    const ORDER_PREFIX = 'order.';

    const ROUTE_QUICK_BUY = self::ORDER_PREFIX . 'quick_buy';


}
