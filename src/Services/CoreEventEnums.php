<?php

namespace VmdCms\Modules\Orders\Services;

class CoreEventEnums
{
    const USER_ORDER_QUICK_CREATE = 'user.order_quick.create';
    const USER_ORDER_CREATE = 'user.order.create';
    const USER_BASKET_ACTION = 'user.basket.action';
    const USER_CHECKOUT_ACTION = 'user.checkout.action';
}
