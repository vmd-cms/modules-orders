<?php

namespace VmdCms\Modules\Orders\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Orders\Models\OrderItem;

interface OrderItemDTOInterface extends Arrayable
{
    /**
     * OrderItemDTOInterface constructor.
     * @param OrderItem $model
     */
    public function __construct(OrderItem $model);

}
