<?php

namespace VmdCms\Modules\Orders\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface BasketDTOCollectionInterface extends CollectionInterface,Arrayable
{
    /**
     * @param BasketDTOInterface $dto
     */
    public function append(BasketDTOInterface $dto);
}
