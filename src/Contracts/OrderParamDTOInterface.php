<?php

namespace VmdCms\Modules\Orders\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Orders\Models\Params\OrderParam;

interface OrderParamDTOInterface extends Arrayable
{
    /**
     * OrderParamDTOInterface constructor.
     * @param OrderParam $model
     */
    public function __construct(OrderParam $model);

}
