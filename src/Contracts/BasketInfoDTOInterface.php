<?php

namespace VmdCms\Modules\Orders\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

interface BasketInfoDTOInterface extends Arrayable
{
    /**
     * BasketInfoDTOInterface constructor.
     * @param BasketDTOCollectionInterface $items
     */
    public function __construct(BasketDTOCollectionInterface $items);

    /**
     * @return int
     */
    public function getTotalQuantity(): int;

    /**
     * @return float
     */
    public function getTotalSum(): float;

    /**
     * @return float
     */
    public function getTotalSumFinal(): float;

    /**
     * @return null|string
     */
    public function getBasketPageLink(): ?string;

    /**
     * @return Collection
     */
    public function getBasketItems(): Collection;
}
