<?php

namespace VmdCms\Modules\Orders\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Orders\Models\Order;

interface OrderDTOInterface extends Arrayable
{
    /**
     * OrderDTOInterface constructor.
     * @param Order $model
     */
    public function __construct(Order $model);

}
