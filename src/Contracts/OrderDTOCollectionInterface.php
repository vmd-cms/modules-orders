<?php

namespace VmdCms\Modules\Orders\Contracts;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface OrderDTOCollectionInterface extends CollectionInterface
{
    /**
     * @param OrderDTOInterface $dto
     */
    public function append(OrderDTOInterface $dto);
}
