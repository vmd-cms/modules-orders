<?php

namespace VmdCms\Modules\Orders\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface OrderItemDTOCollectionInterface extends CollectionInterface, Arrayable
{
    /**
     * @param OrderItemDTOInterface $dto
     */
    public function append(OrderItemDTOInterface $dto);
}
