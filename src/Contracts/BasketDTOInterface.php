<?php

namespace VmdCms\Modules\Orders\Contracts;

use VmdCms\Modules\Orders\Models\Basket;

interface BasketDTOInterface
{
    /**
     * BasketDTOInterface constructor.
     * @param Basket $model
     */
    public function __construct(Basket $model);

    /**
     * @return int
     */
    public function getQuantity(): int;

    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @return float
     */
    public function getPriceFinal(): float;

    /**
     * @return float
     */
    public function getPriceWithAction(): float;

    /**
     * @return float
     */
    public function getPriceWithUserDiscount(): float;

    /**
     * @return float
     */
    public function getTotalSum(): float;

    /**
     * @return float
     */
    public function getTotalSumFinal(): float;

    /**
     * @return float
     */
    public function getTotalSumWithAction(): float;

    /**
     * @return float
     */
    public function getTotalSumWithUserDiscount(): float;

    /**
     * @return float
     */
    public function getTotalActionDiscount(): float;

    /**
     * @return float
     */
    public function getTotalUserDiscount(): float;

    /**
     * @return bool
     */
    public function hasActionPrice(): bool;

    /**
     * @return bool
     */
    public function hasUserDiscountPrice(): bool;
}
