<?php

namespace VmdCms\Modules\Orders\DTO;

use App\Modules\Prices\DTO\PriceDTO;
use App\Modules\Prices\Entity\PriceEntity;
use App\Modules\Products\DTO\ProductDTO;
use VmdCms\Modules\Orders\Contracts\BasketDTOInterface;
use VmdCms\Modules\Orders\Models\Basket;
use VmdCms\Modules\Prices\Models\Price;
use VmdCms\Modules\Products\Models\Product;

class BasketItemDTO implements BasketDTOInterface
{
    /**
     * @var PriceDTO
     */
    protected $priceDTO;

    /**
     * @var ProductDTO
     */
    protected $productDTO;

    protected $basketItemId;
    protected $priceId;
    protected $productId;

    protected $quantity;

    protected $hasActionPrice;
    protected $hasUserDiscountPrice;

    protected $totalActionDiscount;
    protected $totalUserDiscount;

    protected $totalSumBase;
    protected $totalSum;
    protected $totalSumFinal;
    protected $totalSumWithAction;
    protected $totalSumWithUserDiscount;

    protected $basePrice;
    protected $price;
    protected $priceFinal;
    protected $priceWithAction;
    protected $priceWithUserDiscount;

    public function __construct(Basket $model)
    {

        if(!$model->price instanceof Price || !$model->price->product instanceof Product) return;

        $this->basketItemId = $model->id;
        $this->priceId = $model->price->id;
        $this->productId = $model->price->products_id;

        $this->quantity = $model->quantity;

        $this->priceDTO = new PriceDTO($model->price);

        $this->setTotalSums();

        $this->productDTO = new ProductDTO($model->price->product);
    }

    /**
     * @return int
     */
    public function getBasketItemId(): int
    {
        return $this->basketItemId;
    }

    /**
     * @return int
     */
    public function getPriceId(): int
    {
        return $this->priceId;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return null|int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getPriceBase(): float
    {
        return $this->basePrice;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getPriceFinal(): float
    {
        return $this->priceFinal;
    }

    /**
     * @return float
     */
    public function getPriceWithAction(): float
    {
        return $this->priceWithAction;
    }

    /**
     * @return float
     */
    public function getPriceWithUserDiscount(): float
    {
        return $this->priceWithUserDiscount;
    }

    /**
     * @return float
     */
    public function getTotalSum(): float
    {
        return $this->totalSum;
    }

    /**
     * @return float
     */
    public function getTotalSumFinal(): float
    {
        return $this->totalSumFinal;
    }

    /**
     * @return float
     */
    public function getTotalSumWithAction(): float
    {
        return $this->totalSumWithAction;
    }

    /**
     * @return float
     */
    public function getTotalSumWithUserDiscount(): float
    {
        return $this->totalSumWithUserDiscount;
    }

    /**
     * @return PriceDTO
     */
    public function getPriceDTO(): PriceDTO
    {
        return $this->priceDTO;
    }

    /**
     * @return ProductDTO
     */
    public function getProductDTO(): ProductDTO
    {
        return $this->productDTO;
    }

    /**
     * @return float
     */
    public function getTotalActionDiscount(): float
    {
        return $this->totalActionDiscount;
    }

    /**
     * @return float
     */
    public function getTotalUserDiscount(): float
    {
        return $this->totalUserDiscount;
    }

    /**
     * @return bool
     */
    public function hasActionPrice(): bool
    {
        return $this->hasActionPrice;
    }

    /**
     * @return bool
     */
    public function hasUserDiscountPrice(): bool
    {
        return $this->hasUserDiscountPrice;
    }

    protected function setTotalSums()
    {
        $this->hasActionPrice = $this->priceDTO->hasActionPrice();
        $this->hasUserDiscountPrice = $this->priceDTO->hasUserDiscountPrice();

        $this->basePrice = $this->priceDTO->getPriceBase();
        $this->price = $this->priceDTO->getPrice();
        $this->priceFinal = $this->priceDTO->getPriceFinal();
        $this->priceWithAction = $this->priceDTO->getPriceWithAction();
        $this->priceWithUserDiscount = $this->priceDTO->getPriceWithUserDiscount();

        $this->totalSumBase = $this->quantity *  $this->basePrice;
        $this->totalSum = $this->quantity *  $this->price;
        $this->totalSumFinal = $this->quantity *  $this->priceFinal;
        $this->totalSumWithAction = $this->quantity * $this->priceWithAction;
        $this->totalSumWithUserDiscount = $this->quantity * $this->priceWithUserDiscount;

        $this->totalActionDiscount = $this->totalSum - $this->totalSumWithAction;
        $this->totalUserDiscount = $this->totalSum - $this->totalSumWithUserDiscount;
    }

    public function toArray()
    {
        return [
            'basketItemId' => $this->getBasketItemId(),
            'priceId' => $this->priceId,
            'productId' => $this->productId,
            'quantity' => $this->getQuantity(),
            'hasActionPrice' => $this->hasActionPrice(),
            'hasUserDiscountPrice' => $this->hasUserDiscountPrice(),
            'totalActionDiscount' => $this->totalActionDiscount,
            'totalUserDiscount' => $this->totalUserDiscount,
            'priceBase' => $this->basePrice,
            'price' => $this->price,
            'priceFinal' => $this->priceFinal,
            'priceWithAction' => $this->priceWithAction,
            'priceWithUserDiscount' => $this->priceWithUserDiscount,
            'totalSumBase' => $this->totalSumBase,
            'totalSum' => $this->totalSum,
            'totalSumFinal' => $this->totalSum,
            'totalSumWithAction' => $this->totalSumWithAction,
            'totalSumWithUserDiscount' => $this->totalSumWithUserDiscount,
        ];
    }
}
