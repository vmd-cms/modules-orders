<?php

namespace VmdCms\Modules\Orders\DTO;

use App\Modules\Prices\DTO\PriceDTO;
use App\Modules\Products\DTO\ProductDTO;
use VmdCms\Modules\Orders\Contracts\OrderItemDTOInterface;
use VmdCms\Modules\Orders\Models\OrderItem;
use VmdCms\Modules\Prices\Contracts\PriceDTOInterface;
use VmdCms\Modules\Products\Contracts\ProductDTOInterface;
use VmdCms\Modules\Products\Models\Product;

class OrderItemDTO implements OrderItemDTOInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var PriceDTOInterface
     */
    protected $priceDTO;

    /**
     * @var ProductDTOInterface
     */
    protected $productDTO;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var float
     */
    protected $priceBase;

    /**
     * @var float
     */
    protected $price;

    /**
     * @var float
     */
    protected $total;

    /**
     * @var float
     */
    protected $totalBase;

    /**
     * @var array
     */
    protected $priceData;

    /**
     * @var array
     */
    protected $productData;

    /**
     * OrderDTO constructor.
     * @param OrderItem $model
     */
    public function __construct(OrderItem $model)
    {
        $this->id = $model->id;
        $this->priceDTO = new PriceDTO($model->priceRelation);
        $this->productDTO = $model->product instanceof Product ? new ProductDTO($model->product) : null;
        $this->quantity = $model->quantity;
        $this->priceBase = $model->price_base;
        $this->price = $model->price;
        $this->total = $model->total;
        $this->priceData = json_decode($model->price_data,true);
        $this->productData = json_decode($model->product_data,true);
        $this->totalBase = $this->priceBase * $this->quantity;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return PriceDTOInterface
     */
    public function getPriceDTO(): PriceDTOInterface
    {
        return $this->priceDTO;
    }

    /**
     * @return ProductDTOInterface|null
     */
    public function getProductDTO(): ?ProductDTOInterface
    {
        return $this->productDTO;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getPriceBase(): float
    {
        return $this->priceBase;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @return float
     */
    public function getTotalBase(): float
    {
        return $this->totalBase;
    }

    /**
     * @return array
     */
    public function getPriceData(): array
    {
        return is_array($this->priceData) ? $this->priceData : [];
    }

    /**
     * @return array
     */
    public function getProductData(): array
    {
        return is_array($this->productData) ? $this->productData : [];
    }

    public function toArray()
    {
        return [
            'order_item_id' => $this->id,
            'priceDTO' => $this->priceDTO->toArray(),
            'productDTO' => $this->productDTO->toArray(),
            'priceData' => $this->priceData,
            'productData' => $this->productData,
        ];
    }
}
