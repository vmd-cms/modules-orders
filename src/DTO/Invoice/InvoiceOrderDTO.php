<?php

namespace VmdCms\Modules\Orders\DTO\Invoice;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Orders\Models\Mocks\InvoiceOrderItemMock;
use VmdCms\Modules\Orders\Models\OrderInvoice;

class InvoiceOrderDTO implements Arrayable
{

    /**
     * @var int
     */
    protected $orderId;

    /**
     * @var int
     */
    protected $moderatorId;

    /**
     * @var string|null
     */
    protected $date;

    /**
     * @var string|null
     */
    protected $orderNumber;

    /**
     * @var string|null
     */
    protected $orderDate;

    /**
     * @var string|null
     */
    protected $orderDeliveryDate;

    /**
     * @var string|null
     */
    protected $orderCustomerName;

    /**
     * @var string|null
     */
    protected $orderCustomerPhone;

    /**
     * @var string|null
     */
    protected $orderDeliveryAddress;

    /**
     * @var string|null
     */
    protected $orderDeliveryContactName;

    /**
     * @var string|null
     */
    protected $orderDeliveryContactPhone;

    /**
     * @var InvoiceOrderItemDTOCollection
     */
    protected $orderItems;

    /**
     * @var string|null
     */
    protected $orderTotal;

    /**
     * @var string|null
     */
    protected $orderTotalStr;

    /**
     * @var string|null
     */
    protected $orderDeliveryType;

    /**
     * @var string|null
     */
    protected $orderPaymentType;

    /**
     * @var string|null
     */
    protected $orderResponsibleManager;

    /**
     * @var string|null
     */
    protected $orderCustomerApprove;

    /**
     * @var string|null
     */
    protected $logoBase64;


    public function __construct(OrderInvoice $model)
    {
        $this->logoBase64 = $model->logo_base_64;
        $this->orderId = $model->order_id;
        $this->moderator_id = $model->moderator_id;
        $this->date = $model->date ?? null;
        $this->orderNumber = $model->order_number ?? null;
        $this->orderDate = $model->order_date ?? null;
        $this->orderDeliveryDate = $model->order_delivery_date ?? null;
        $this->orderCustomerName = $model->order_customer_name ?? null;
        $this->orderCustomerPhone = $model->order_customer_phone ?? null;
        $this->orderDeliveryAddress = $model->order_delivery_address ?? null;
        $this->orderDeliveryContactName = $model->order_delivery_contact_name ?? null;
        $this->orderDeliveryContactPhone = $model->order_delivery_contact_phone ?? null;
        $this->orderQuantity = $model->order_quantity ?? null;
        $this->orderTotal = $model->order_total ?? null;
        $this->orderTotalStr = $model->order_total_str ?? null;
        $this->orderDeliveryType = $model->order_delivery_type ?? null;
        $this->orderPaymentType = $model->order_payment_type ?? null;
        $this->orderResponsibleManager = $model->order_responsible_manager ?? null;
        $this->orderCustomerApprove = $model->order_customer_approve ?? null;

        $this->mapOrderItems($model->order_items ?? []);
    }

    protected function mapOrderItems(array $orderItems = [])
    {
        $this->orderItems = new InvoiceOrderItemDTOCollection();
        if(is_countable($orderItems)){
            foreach ($orderItems as $item){
                $this->orderItems->append(new InvoiceOrderItemDTO(new InvoiceOrderItemMock($item)));
            }
        }
    }

    /**
     * @return string
     */
    public function getLogoBase64()
    {
        return $this->logoBase64;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return int
     */
    public function getModeratorId(): int
    {
        return $this->moderatorId;
    }

    /**
     * @return string|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string|null
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return string|null
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @return string|null
     */
    public function getOrderDeliveryDate()
    {
        return $this->orderDeliveryDate;
    }

    /**
     * @return string|null
     */
    public function getOrderCustomerName()
    {
        return $this->orderCustomerName;
    }

    /**
     * @return string|null
     */
    public function getOrderCustomerPhone()
    {
        return $this->orderCustomerPhone;
    }

    /**
     * @return string|null
     */
    public function getOrderDeliveryAddress()
    {
        return $this->orderDeliveryAddress;
    }

    /**
     * @return string|null
     */
    public function getOrderDeliveryContactName()
    {
        return $this->orderDeliveryContactName;
    }

    /**
     * @return string|null
     */
    public function getOrderDeliveryContactPhone()
    {
        return $this->orderDeliveryContactPhone;
    }

    /**
     * @return InvoiceOrderItemDTOCollection
     */
    public function getOrderItems(): InvoiceOrderItemDTOCollection
    {
        return $this->orderItems;
    }

    /**
     * @return string|null
     */
    public function getOrderTotal()
    {
        return $this->orderTotal;
    }

    /**
     * @return string|null
     */
    public function getOrderTotalStr()
    {
        return $this->orderTotalStr;
    }

    /**
     * @return string|null
     */
    public function getOrderDeliveryType()
    {
        return $this->orderDeliveryType;
    }

    /**
     * @return string|null
     */
    public function getOrderPaymentType()
    {
        return $this->orderPaymentType;
    }

    /**
     * @return string|null
     */
    public function getOrderResponsibleManager()
    {
        return $this->orderResponsibleManager;
    }

    /**
     * @return string|null
     */
    public function getOrderCustomerApprove()
    {
        return $this->orderCustomerApprove;
    }

    public function toArray()
    {
        return [
            'logo_base_64' => $this->logoBase64,
            'date' => $this->date,
            'order_number' => $this->orderNumber,
            'order_date' => $this->orderDate,
            'order_delivery_date' => $this->orderDeliveryDate,
            'order_customer_name' => $this->orderCustomerName,
            'order_customer_phone' => $this->orderCustomerPhone,
            'order_delivery_address' => $this->orderDeliveryAddress,
            'order_delivery_contact_name' => $this->orderDeliveryContactName,
            'order_delivery_contact_phone' => $this->orderDeliveryContactPhone,
            'order_total' => $this->orderTotal,
            'order_total_str' => $this->orderTotalStr,
            'order_delivery_type' => $this->orderDeliveryType,
            'order_payment_type' => $this->orderPaymentType,
            'order_responsible_manager' => $this->orderResponsibleManager,
            'order_customer_approve' => $this->orderCustomerApprove,
            'order_items' => $this->orderItems->toArray(),
        ];
    }
}
