<?php

namespace VmdCms\Modules\Orders\DTO\Invoice;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class InvoiceOrderItemDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param InvoiceOrderItemDTO $dto
     */
    public function append(InvoiceOrderItemDTO $dto)
    {
        $this->collection->add($dto);
    }

    public function toArray()
    {
        $itemsArr = [];
        foreach ($this->collection as $item){
            $itemsArr[] = $item->toArray();
        }
        return $itemsArr;
    }
}
