<?php

namespace VmdCms\Modules\Orders\DTO\Invoice;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Orders\Models\Mocks\InvoiceOrderItemMock;

class InvoiceOrderItemDTO implements Arrayable
{
    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $title;

    /**
     * @var int|null
     */
    protected $quantity;

    /**
     * @var string|null
     */
    protected $price;

    /**
     * @var string|null
     */
    protected $total;

    public function __construct(InvoiceOrderItemMock $model)
    {
        $this->id = $model->id ?? null;
        $this->title = $model->title ?? null;
        $this->quantity = $model->quantity ?? null;
        $this->price = $model->price ?? null;
        $this->total = $model->total ?? null;
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string|null
     */
    public function getTotal()
    {
        return $this->total;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'total' => $this->total
        ];
    }
}
