<?php

namespace VmdCms\Modules\Orders\DTO;

use VmdCms\Modules\Orders\Contracts\OrderParamDTOInterface;
use VmdCms\Modules\Orders\Models\Params\OrderParam;

class OrderParamDTO implements OrderParamDTOInterface
{
    protected $id;
    protected $key;
    protected $slug;
    protected $active;
    protected $title;
    protected $descriptionShort;
    protected $description;

    public function __construct(OrderParam $model){
        $this->id = $model->id;
        $this->key = $model->key;
        $this->slug = $model->slug;
        $this->active = $model->active;
        $this->title = $model->info->title ?? null;
        $this->descriptionShort = $model->info->description_short ?? null;
        $this->description = $model->info->description ?? null;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->key;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getDescriptionShort(): ?string
    {
        return $this->descriptionShort;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'key' => $this->key,
            'slug' => $this->slug,
            'active' => $this->active,
            'title' => $this->title,
            'descriptionShort' => $this->descriptionShort,
            'description' => $this->description
        ];
    }
}
