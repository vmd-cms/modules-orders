<?php

namespace VmdCms\Modules\Orders\DTO;

use VmdCms\Modules\Orders\Collections\OrderItemDTOCollection;
use VmdCms\Modules\Orders\Contracts\OrderDTOInterface;
use VmdCms\Modules\Orders\Contracts\OrderItemDTOCollectionInterface;
use VmdCms\Modules\Orders\Models\Order;
use VmdCms\Modules\Prices\Contracts\CurrencyDTOInterface;
use VmdCms\Modules\Prices\DTO\CurrencyDTO;
use VmdCms\Modules\Users\Contracts\UserDTOInterface;
use VmdCms\Modules\Users\DTO\UserDeliveryDTO;
use VmdCms\Modules\Users\DTO\UserDTO;
use VmdCms\Modules\Users\Models\UserDelivery;

class OrderDTO implements OrderDTOInterface
{
    protected $id;

    /**
     * @var OrderItemDTOCollectionInterface
     */
    protected $orderItems;

    /**
     * @var UserDTOInterface
     */
    protected $userDTO;

    /**
     * @var CurrencyDTOInterface
     */
    protected $currencyDTO;

    /**
     * @var OrderParamDTO
     */
    protected $paymentTypeDTO;
    protected $deliveryTypeDTO;
    protected $orderStatusDTO;

    /**
     * @var UserDeliveryDTO
     */
    protected $deliveryDTO;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var array
     */
    protected $orderData;

    /**
     * @var \Illuminate\Support\Carbon
     */
    protected $createdAt;

    /**
     * @var float
     */
    protected $totalSum;

    /**
     * @var float
     */
    protected $totalBaseSum;

    /**
     * @var float
     */
    protected $totalDiscountSum;

    public function __construct(Order $model)
    {
        $this->id = $model->id;
        $this->createdAt = $model->created_at;
        $this->setOrderItems($model);
        $this->userDTO = new UserDTO($model->user);
        $this->currencyDTO = new CurrencyDTO(json_decode(json_encode($model->currency)));
        $this->paymentTypeDTO = new OrderParamDTO($model->paymentType);
        $this->deliveryTypeDTO = new OrderParamDTO($model->deliveryType);
        $this->orderStatusDTO = new OrderParamDTO($model->status);
        $this->comment = $model->comment;
        $this->orderData = json_decode($model->order_data,true);
        $this->setTotalSums();
        $this->mapDeliveryDTO($model);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return OrderItemDTOCollectionInterface
     */
    public function getOrderItems(): OrderItemDTOCollectionInterface
    {
        return $this->orderItems;
    }

    /**
     * @return UserDTOInterface
     */
    public function getUserDTO(): UserDTOInterface
    {
        return $this->userDTO;
    }

    /**
     * @return CurrencyDTOInterface
     */
    public function getCurrencyDTO(): CurrencyDTOInterface
    {
        return $this->currencyDTO;
    }

    /**
     * @return OrderParamDTO
     */
    public function getPaymentTypeDTO(): OrderParamDTO
    {
        return $this->paymentTypeDTO;
    }

    /**
     * @return null|UserDeliveryDTO
     */
    public function getDeliveryDTO(): ?UserDeliveryDTO
    {
        return $this->deliveryDTO;
    }

    /**
     * @return OrderParamDTO
     */
    public function getDeliveryTypeDTO(): OrderParamDTO
    {
        return $this->deliveryTypeDTO;
    }

    /**
     * @return OrderParamDTO
     */
    public function getOrderStatusDTO(): OrderParamDTO
    {
        return $this->orderStatusDTO;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return array
     */
    public function getOrderData(): array
    {
        return $this->orderData;
    }

    /**
     * @return float
     */
    public function getTotalSum(): float
    {
        return $this->totalSum;
    }

    /**
     * @return float
     */
    public function getTotalBaseSum(): float
    {
        return $this->totalBaseSum;
    }

    /**
     * @return float
     */
    public function getTotalDiscountSum(): float
    {
        return is_float($this->totalDiscountSum) ? $this->totalDiscountSum : 0;
    }

    /**
     * @return \Illuminate\Support\Carbon
     */
    public function getCreatedAt(): \Illuminate\Support\Carbon
    {
        return $this->createdAt;
    }

    public function toArray()
    {
        return [
            'order_id' => $this->id,
            'user' => $this->userDTO->toArray(),
            'currency' => $this->currencyDTO->toArray(),
            'payment_type' => $this->paymentTypeDTO->toArray(),
            'delivery_types' => $this->deliveryTypeDTO->toArray(),
            'order_status' => $this->orderStatusDTO->toArray(),
            'order_data' => $this->orderData,
            'comment' => $this->comment,
        ];
    }

    protected function setOrderItems(Order $model)
    {
        $this->orderItems = new OrderItemDTOCollection();
        if(isset($model->items) && is_countable($model->items))
        {
            foreach ($model->items as $item)
            {
                $this->orderItems->append(new OrderItemDTO($item));
            }
        }
    }

    protected function setTotalSums(){
        $this->totalSum = 0;
        foreach ($this->orderItems->getItems() as $item){
            $this->totalSum += $item->getTotal();
            $this->totalBaseSum += $item->getTotalBase();
            $this->totalDiscountSum += $item->getTotalBase() - $item->getTotal();
        }
    }

    /**
     * @param Order $order
     */
    protected function mapDeliveryDTO(Order $order)
    {
        $delivery = !empty($order->delivery_data) ? json_decode($order->delivery_data,true) : null;
        $userDelivery = null;
        if(is_array($delivery)){
            $userDelivery = new UserDelivery();
            $userDelivery->order_param_id = isset($delivery['delivery_type']) ? intval($delivery['delivery_type']) : null;
            $userDelivery->city_title = isset($delivery['delivery_city_title']) ? $delivery['delivery_city_title'] : null;
            $userDelivery->city_ref = isset($delivery['delivery_city_ref']) ? $delivery['delivery_city_ref'] : null;
            $userDelivery->city_delivery_id = isset($delivery['delivery_city_id']) ? $delivery['delivery_city_id'] : null;
            $userDelivery->warehouse_title = isset($delivery['delivery_warehouse_title']) ? $delivery['delivery_warehouse_title'] : null;
            $userDelivery->warehouse_ref = isset($delivery['delivery_warehouse_ref']) ? $delivery['delivery_warehouse_ref'] : null;
        }
        $this->deliveryDTO = $userDelivery instanceof UserDelivery ? new UserDeliveryDTO($userDelivery) : null;
    }

}
