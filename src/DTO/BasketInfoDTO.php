<?php

namespace VmdCms\Modules\Orders\DTO;

use App\Modules\Orders\Services\OrderRouter;
use Illuminate\Support\Collection;
use VmdCms\Modules\Orders\Contracts\BasketDTOCollectionInterface;
use VmdCms\Modules\Orders\Contracts\BasketInfoDTOInterface;

class BasketInfoDTO implements BasketInfoDTOInterface
{
    protected $basketPageLink;
    protected $totalQuantity;
    protected $totalSum;
    protected $totalSumFinal;

    /**
     * @var BasketDTOCollectionInterface
     */
    protected $basketItems;

    public function __construct(BasketDTOCollectionInterface $items)
    {
        $this->basketItems = $items;
        $this->calculateTotal();
        $this->basketPageLink = route(OrderRouter::ROUTE_BASKET,[],false);
    }

    /**
     * @return int
     */
    public function getTotalQuantity(): int
    {
        return $this->totalQuantity;
    }

    /**
     * @return float
     */
    public function getTotalSum(): float
    {
        return $this->totalSum;
    }

    /**
     * @return float
     */
    public function getTotalSumFinal(): float
    {
        return $this->totalSumFinal;
    }

    /**
     * @return null|string
     */
    public function getBasketPageLink(): ?string
    {
        return $this->basketPageLink;
    }

    /**
     * @return Collection
     */
    public function getBasketItems(): Collection
    {
        return $this->basketItems->getItems();
    }

    public function toArray()
    {
        return [
            'basketPageLink' => $this->basketPageLink,
            'totalSum' => $this->totalSum,
            'totalSumFinal' => $this->totalSumFinal,
            'totalQuantity' => $this->totalQuantity,
            'basketItems' => $this->getBasketItems(),
        ];
    }

    protected function calculateTotal()
    {
        $quantity = $sum = $sumFinal = 0;
        foreach ($this->getBasketItems() as $item)
        {
            $quantity += $item->getQuantity();
            $sum += $item->getTotalSum();
            $sumFinal += $item->getTotalSumFinal();
        }
        $this->totalQuantity = $quantity;
        $this->totalSum = $sum;
        $this->totalSumFinal = $sumFinal;
    }
}
