<?php

use VmdCms\Modules\Orders\Models\OrderInvoice as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('moderator_id')->unsigned()->nullable();
            $table->longText('data')->nullable();
            $table->longText('content')->nullable();
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('order_id', model::table() . '_order_id_fk')
                ->references('id')->on('orders')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('moderator_id', model::table() . '_moderator_id_fk')
                ->references('id')->on(\VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
