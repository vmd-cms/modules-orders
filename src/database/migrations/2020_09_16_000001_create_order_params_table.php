<?php

use App\Modules\Orders\Models\Params\OrderParam as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('key',32)->nullable();
            $table->string('slug',64)->nullable();
            $table->string('resource')->nullable();
            $table->integer('resource_id')->nullable()->unsigned();
            $table->text('data')->nullable();
            $table->boolean('active')->default(1);
            $table->integer('order')->unsigned()->default(1);
            $table->timestamps();

            $table->unique(['key','slug']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
