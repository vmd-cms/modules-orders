<?php

use App\Modules\Orders\Models\QuickOrder as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuickOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('currency_id')->unsigned()->nullable();
            $table->integer('status_id')->nullable()->unsigned();
            $table->string('currency_title')->nullable();
            $table->integer('price_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->string('phone')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->integer('quantity')->default(1);
            $table->float('price_base')->unsigned();
            $table->float('price_discount')->unsigned();
            $table->text('product_data')->nullable();
            $table->text('price_data')->nullable();
            $table->float('total')->unsigned();
            $table->boolean('checked')->default(false);
            $table->integer('moderator_id')->unsigned()->nullable();
            $table->timestamp('checked_at')->nullable();
            $table->boolean('deleted')->default(false);
            $table->integer('deleted_by')->unsigned()->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('user_id', model::table() . '_user_id_fk')
                ->references('id')->on(\App\Modules\Users\Models\User::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('currency_id', model::table() . '_currency_id_fk')
                ->references('id')->on(\App\Modules\Prices\Models\Currency::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('price_id', model::table() . '_price_id_fk')
                ->references('id')->on(\App\Modules\Prices\Models\Price::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('product_id', model::table() . '_product_id_fk')
                ->references('id')->on(\App\Modules\Products\Models\Product::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('moderator_id', model::table() . '_moderator_id_fk')
                ->references('id')->on(\VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('status_id', model::table() . '_status_id_fk')
                ->references('id')->on(\App\Modules\Orders\Models\Params\OrderStatusParam::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
