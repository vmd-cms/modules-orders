<?php

use VmdCms\Modules\Orders\Models\OrderHistory as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('moderator_id')->unsigned()->nullable();
            $table->string('action',32)->nullable();
            $table->string('value',1024)->nullable();
            $table->boolean('is_moderator')->default(false);
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('order_id', model::table() . '_order_id_fk')
                ->references('id')->on('orders')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('user_id', model::table() . '_user_id_fk')
                ->references('id')->on(\VmdCms\Modules\Users\Models\User::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('moderator_id', model::table() . '_moderator_id_fk')
                ->references('id')->on(\VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
