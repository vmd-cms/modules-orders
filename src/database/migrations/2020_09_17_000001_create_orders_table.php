<?php

use App\Modules\Orders\Models\Order as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('currency_id')->unsigned()->nullable();
            $table->string('currency_title')->nullable();
            $table->integer('payment_type_id')->unsigned()->nullable();
            $table->integer('delivery_type_id')->unsigned()->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->text('order_data')->nullable();
            $table->text('delivery_data')->nullable();
            $table->text('payment_data')->nullable();
            $table->text('user_data')->nullable();
            $table->string('comment',2000)->nullable();
            $table->integer('moderator_id')->unsigned()->nullable();
            $table->boolean('is_payed')->default(false);
            $table->boolean('deleted')->default(false);
            $table->integer('deleted_by')->unsigned()->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('user_id', model::table() . '_user_id_fk')
                ->references('id')->on(\App\Modules\Users\Models\User::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('currency_id', model::table() . '_currency_id_fk')
                ->references('id')->on(\App\Modules\Prices\Models\Currency::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('payment_type_id', model::table() . '_payment_type_id_fk')
                ->references('id')->on(\App\Modules\Orders\Models\Params\PaymentTypeParam::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('delivery_type_id', model::table() . '_delivery_type_id_fk')
                ->references('id')->on(\App\Modules\Orders\Models\Params\DeliveryTypeParam::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('status_id', model::table() . '_status_id_fk')
                ->references('id')->on(\App\Modules\Orders\Models\Params\OrderStatusParam::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('moderator_id', model::table() . '_moderator_id_fk')
                ->references('id')->on(\VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
