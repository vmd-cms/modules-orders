<?php

use App\Modules\Orders\Models\Basket as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('session_id',128)->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('price_id')->unsigned();
            $table->integer('quantity')->default(1)->unsigned();
            $table->integer('order')->default(1)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
