<?php

use App\Modules\Orders\Models\OrderItem as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('price_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('quantity')->default(1);
            $table->float('price_base')->unsigned();
            $table->float('price')->unsigned();
            $table->text('product_data')->nullable();
            $table->text('price_data')->nullable();
            $table->float('total')->unsigned();
            $table->integer('order')->unsigned();
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('order_id', model::table() . '_order_id_fk')
                ->references('id')->on('orders')
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('price_id', model::table() . '_price_id_fk')
                ->references('id')->on(\App\Modules\Prices\Models\Price::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('product_id', model::table() . '_product_id_fk')
                ->references('id')->on(\App\Modules\Products\Models\Product::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
