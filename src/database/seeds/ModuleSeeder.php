<?php

namespace VmdCms\Modules\Orders\database\seeds;

use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;
use App\Modules\Orders\Sections\Order;
use App\Modules\Orders\Sections\Components\OrderGroupBlock;
use App\Modules\Orders\Sections\Components\OrderTranslate;
use App\Modules\Orders\Sections\QuickOrder;
use VmdCms\Modules\Orders\Sections\OrderInvoice;


class ModuleSeeder extends AbstractModuleSeeder
{

    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            Order::class => "Заказы",
            QuickOrder::class => "Быстрые Заказы",
            OrderGroupBlock::class => "Инфоблоки Заказы",
            OrderTranslate::class => "Переводы Заказы",
            OrderInvoice::class => "Инвойсы",
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "shop_group",
                "title" => "Магазин",
                "is_group_title" => true,
                "order" => 3,
                "children" => [
                    [
                        "slug" => "orders",
                        "title" => "Заказы",
                        "icon" => "icon icon-order",
                        "section_class" => Order::class
                    ],
                    [
                        "slug" => "quick_orders",
                        "title" => "Быстрые заказы",
                        "icon" => "icon icon-order",
                        "section_class" => QuickOrder::class
                    ]
                ]
            ],
            [
                "slug" => "content_group",
                "title" => "Контент",
                "is_group_title" => true,
                "order" => 3,
                "children" => [
                    [
                        "slug" => "block_groups",
                        "title" => "Инфоблоки",
                        "icon" => "icon icon-copy",
                        "children" => [
                            [
                                "slug" => "order_blocks",
                                "title" => "Заказы",
                                "section_class" => OrderGroupBlock::class,
                                "order" => 4,
                            ]
                        ]
                    ]
                ]
            ],
            [
                "slug" => "configs",
                "title" => "Конфигурации",
                "is_group_title" => true,
                "order" => 5,
                "children" => [
                    [
                        "slug" => "translate_groups",
                        "title" => "Переводы",
                        "order" => 2,
                        "icon" => "icon icon-card",
                        "children" => [
                            [
                                "slug" => "order_translates",
                                "title" => "Заказы",
                                "section_class" => OrderTranslate::class,
                                "order" => 5
                            ],
                        ]
                    ]
                ]
            ]
        ];
    }

}
