<?php

namespace VmdCms\Modules\Orders\Models;

use App\Modules\Prices\Models\Currency;
use App\Modules\Users\Models\User;
use VmdCms\CoreCms\Contracts\Models\SoftDeletableInterface;
use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\CoreCms\Traits\Models\SoftDeletable;
use VmdCms\Modules\Orders\Models\Params\DeliveryTypeParam;
use VmdCms\Modules\Orders\Models\Params\OrderStatusParam;
use VmdCms\Modules\Orders\Models\Params\PaymentTypeParam;

class Order extends CmsModel implements SoftDeletableInterface
{
    use SoftDeletable;

    public static function table(): string
    {
       return 'orders';
    }

    protected function getModelDataField()
    {
        return $this->order_data ?? null;
    }

    public function getCustomerNameAttribute()
    {
        return $this->user instanceof \VmdCms\Modules\Users\Models\User ? $this->user->last_name . ' ' . $this->user->first_name : null;
    }

    public function getOrderTotalAttribute(){
        $total = 0;
        foreach ($this->items as $item){
            $total += $item->total;
        }
        return $total;
    }

    public function getOrderQuantityAttribute(){
        $quantity = 0;
        foreach ($this->items as $item){
            $quantity += $item->quantity;
        }
        return $quantity;
    }

    public function getOrderSumAttribute()
    {
        return $this->order_total . ' ' . $this->currency->info->symbol;
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function moderator(){
        return $this->belongsTo(Moderator::class,'moderator_id','id');
    }

    public function currency(){
        return $this->belongsTo(Currency::class,'currency_id','id');
    }

    public function paymentType(){
        return $this->belongsTo(PaymentTypeParam::class,'payment_type_id','id')->with('info');
    }

    public function deliveryType(){
        return $this->belongsTo(DeliveryTypeParam::class,'delivery_type_id','id')->with('info');
    }

    public function status(){
        return $this->belongsTo(OrderStatusParam::class,'status_id','id')->with('info');
    }

    public function items()
    {
        return $this->hasMany(\App\Modules\Orders\Models\OrderItem::class,'order_id','id');
    }

    public function history()
    {
        return $this->hasMany(OrderHistory::class,'order_id','id');
    }
}
