<?php

namespace VmdCms\Modules\Orders\Models\Mocks;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\CoreCms\CoreModules\Content\Models\MockCmsModel;

class InvoiceOrderItemMock extends MockCmsModel
{
    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string|null
     */
    public $title;

    /**
     * @var int|null
     */
    public $quantity;

    /**
     * @var string|null
     */
    public $price;

    /**
     * @var string|null
     */
    public $total;

    /**
     * InvoiceOrderItemDTO constructor.
     * @param array $item
     */
    public function __construct(array $item = [])
    {
        $this->id = $item['id'] ?? null;
        $this->title = $item['title'] ?? null;
        $this->quantity = $item['quantity'] ?? null;
        $this->price = $item['price'] ?? null;
        $this->total = $item['total'] ?? null;
    }

    /**
     * @param int|null $id
     * @return InvoiceOrderItemMock
     */
    public function setId(int $id = null)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string|null $title
     * @return InvoiceOrderItemMock
     */
    public function setTitle(string $title = null)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param int|null $quantity
     * @return InvoiceOrderItemMock
     */
    public function setQuantity(int $quantity = null)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @param string|null $price
     * @return InvoiceOrderItemMock
     */
    public function setPrice(string $price = null)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @param string|null $total
     * @return InvoiceOrderItemMock
     */
    public function setTotal(string $total = null)
    {
        $this->total = $total;
        return $this;
    }
}
