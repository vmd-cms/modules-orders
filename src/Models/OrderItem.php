<?php

namespace VmdCms\Modules\Orders\Models;

use App\Modules\Prices\Models\Price;
use App\Modules\Products\Models\Product;
use VmdCms\CoreCms\Contracts\Models\CmsRelatedModelInterface;
use VmdCms\CoreCms\Models\CmsModel;

class OrderItem extends CmsModel implements CmsRelatedModelInterface
{
    public static function table(): string
    {
       return 'order_items';
    }

    public function orderModel()
    {
        return $this->belongsTo(\App\Modules\Orders\Models\Order::class,'order_id','id');
    }

    public function priceRelation()
    {
        return $this->belongsTo(Price::class,'price_id','id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }


    public static function getForeignField(): string
    {
        return 'order_id';
    }

    public static function getBaseModelClass(): string
    {
        return Order::class;
    }
}
