<?php

namespace VmdCms\Modules\Orders\Models;

use App\Modules\Orders\Models\Params\OrderStatusParam;
use App\Modules\Prices\Models\Currency;
use App\Modules\Prices\Models\Price;
use App\Modules\Products\Models\Product;
use App\Modules\Users\Models\User;
use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;
use VmdCms\CoreCms\Models\CmsModel;

class QuickOrder extends CmsModel
{
    public static function table(): string
    {
       return 'quick_orders';
    }

    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function price(){
        return $this->belongsTo(Price::class,'price_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function currency(){
        return $this->belongsTo(Currency::class,'currency_id','id');
    }

    public function status(){
        return $this->belongsTo(OrderStatusParam::class,'status_id','id');
    }

    public function moderator(){
        return $this->belongsTo(Moderator::class,'moderator_id','id');
    }
}
