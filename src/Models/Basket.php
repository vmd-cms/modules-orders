<?php

namespace VmdCms\Modules\Orders\Models;

use App\Modules\Prices\Models\Price;
use VmdCms\CoreCms\Models\CmsModel;

class Basket extends CmsModel
{
    public static function table(): string
    {
       return 'basket';
    }

    public function price()
    {
        return $this->belongsTo(Price::class,'price_id','id')->with('product','priceTaxonomy');
    }
}
