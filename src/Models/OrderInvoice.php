<?php

namespace VmdCms\Modules\Orders\Models;

use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\Modules\Orders\Entity\OrderInvoiceEntity;

class OrderInvoice extends CmsModel
{
    use JsonData;

    public static function table(): string
    {
       return 'order_invoices';
    }

    public function order(){
        return $this->belongsTo(Order::class,'order_id','id');
    }

    public function moderator(){
        return $this->belongsTo(Moderator::class,'moderator_id','id');
    }

    public function getJsonDataFieldsArr() : array
    {
        return [
            'logo_base_64',
            'date',
            'order_number',
            'order_date',
            'order_delivery_date',
            'order_customer_name',
            'order_customer_phone',
            'order_delivery_address',
            'order_delivery_contact_name',
            'order_delivery_contact_phone',
            'order_items',
            'order_total',
            'order_total_str',
            'order_delivery_type',
            'order_payment_type',
            'order_responsible_manager',
            'order_customer_approve',
        ];
    }

    public function getOrderItemsListAttribute(){
        return (new OrderInvoiceEntity())->getOrderItemsCollection($this);
    }

}
