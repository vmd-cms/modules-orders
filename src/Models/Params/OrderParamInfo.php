<?php

namespace VmdCms\Modules\Orders\Models\Params;

use VmdCms\CoreCms\Models\CmsInfoModel;

class OrderParamInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'order_params_info';
    }
}
