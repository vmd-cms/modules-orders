<?php

namespace VmdCms\Modules\Orders\Models\Params;

use App\Modules\Orders\Models\Params\OrderParam;

class DeliveryTypeParam extends OrderParam
{
    const NOVAPOSHTA = 'novaposhta';

    public static function getModelKey(): string
    {
        return 'delivery_type';
    }
}
