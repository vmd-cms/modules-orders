<?php

namespace VmdCms\Modules\Orders\Models\Params;

use App\Modules\Orders\Models\Params\OrderParam;

class OrderStatusParam extends OrderParam
{
    public static function getModelKey(): string
    {
        return 'order_status';
    }

    public function getJsonDataFieldsArr() : array
    {
        return ['color'];
    }
}
