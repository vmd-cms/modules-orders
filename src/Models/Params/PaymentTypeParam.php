<?php

namespace VmdCms\Modules\Orders\Models\Params;

use App\Modules\Orders\Models\Params\OrderParam;

class PaymentTypeParam extends OrderParam
{
    const LIQPAY = 'liqpay';

    public static function getModelKey(): string
    {
        return 'payment_type';
    }
}
