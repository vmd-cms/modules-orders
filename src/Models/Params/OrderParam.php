<?php

namespace VmdCms\Modules\Orders\Models\Params;

use App\Modules\Orders\Models\Params\OrderParamInfo;
use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\CoreCms\Traits\Models\Orderable;

class OrderParam extends CmsModel implements ActivableInterface, HasInfoInterface, OrderableInterface
{
    use Activable,HasInfo,Orderable,JsonData;

    public static function table(): string
    {
        return 'order_params';
    }

    public function setTitleAttribute($value)
    {
        static::saved(function ($model) use ($value){
            if(!$model->info){
                $modelInfo = new OrderParamInfo();
                $modelInfo->title = $value;
                $model->info()->save($modelInfo);
            }
            else{
                $model->info->title = $value;
                $model->info->save();
            }
        });
    }

    public function getTitleAttribute()
    {
        return $this->info->title ?? null;
    }

    /**
     * @return string
     */
    public static function getModelKey() : string
    {}

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('model_key', function (Builder $builder) {
            $builder->where('order_params.key', static::getModelKey());
        });
        static::saving(function (OrderParam $model){
            $model->key = static::getModelKey();
        });

    }

    protected function getInfoClass() : string
    {
        return OrderParamInfo::class;
    }
}
