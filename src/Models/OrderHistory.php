<?php

namespace VmdCms\Modules\Orders\Models;

use App\Modules\Users\Models\User;
use VmdCms\CoreCms\CoreModules\Moderators\Models\Moderator;
use VmdCms\CoreCms\Models\CmsModel;

class OrderHistory extends CmsModel
{
    public static function table(): string
    {
       return 'order_history';
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function moderator(){
        return $this->belongsTo(Moderator::class,'moderator_id','id');
    }

}
