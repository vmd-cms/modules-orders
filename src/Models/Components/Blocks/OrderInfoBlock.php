<?php

namespace VmdCms\Modules\Orders\Models\Components\Blocks;

use VmdCms\Modules\Orders\Models\Components\OrderGroupBlock;
use VmdCms\Modules\Orders\Services\BlockEnum;

class OrderInfoBlock extends OrderGroupBlock
{
    public static function getModelKey(): ?string
    {
        return BlockEnum::ORDER_INFO_BLOCK;
    }

    public function getJsonDataFieldsArr() : array
    {
        return ['title'];
    }
}
