<?php

namespace VmdCms\Modules\Orders\Models\Components;

use VmdCms\CoreCms\CoreModules\Translates\Models\Translate;

class OrderTranslate extends Translate
{
    public static function getModelGroup(): ?string
    {
        return 'order';
    }
}
