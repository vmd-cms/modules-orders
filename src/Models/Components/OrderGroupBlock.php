<?php

namespace VmdCms\Modules\Orders\Models\Components;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;

class OrderGroupBlock extends Block
{
    public static function getModelGroup(): ?string
    {
        return 'order';
    }
}
