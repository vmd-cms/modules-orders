<?php

namespace VmdCms\Modules\Orders\Entity;

use App\Modules\Content\Services\DataShare;
use App\Modules\Orders\DTO\BasketInfoDTO;
use App\Modules\Orders\DTO\BasketItemDTO;
use App\Modules\Orders\Models\Basket;
use App\Modules\Products\Models\Components\ProductTranslate;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use VmdCms\CoreCms\DTO\Dashboard\BreadCrumbDto;
use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Services\Auth\UserSession;
use VmdCms\CoreCms\Services\BreadCrumbs;
use VmdCms\Modules\Orders\Collections\BasketDTOCollection;
use VmdCms\Modules\Orders\Contracts\BasketDTOCollectionInterface;
use VmdCms\Modules\Orders\Exceptions\BasketException;
use VmdCms\Modules\Orders\Exceptions\BasketItemDeletedException;

class BasketEntity
{
    /**
     * @return BasketInfoDTO
     */
    public function getBasketInfoDto(): BasketInfoDTO
    {
        return new BasketInfoDTO($this->getBasketItemsDtoCollection());
    }

    public function shareBasketPageData()
    {
        DataShare::getInstance()->translates->appendGroups([ProductTranslate::getModelGroup()]);
        BreadCrumbs::getInstance()->prependItem(new BreadCrumbDto('',DataShare::getInstance()->getTranslate('basket')));
        DataShare::getInstance()->appendData('breadCrumbs',BreadCrumbs::getInstance()->getItems());
    }

    /**
     * @param int $priceId
     * @param int $quantity
     * @return \VmdCms\Modules\Orders\DTO\BasketItemDTO|null
     * @throws BasketException
     * @throws BasketItemDeletedException
     * @throws ModelNotFoundException
     */
    public function changeBasketItemCount(int $priceId,int $quantity = 1): ?\VmdCms\Modules\Orders\DTO\BasketItemDTO
    {
        $basket = $this->getBasketModel($priceId);
        if(!$basket instanceof \VmdCms\Modules\Orders\Models\Basket){
            throw new ModelNotFoundException();
        }

        if($quantity <= 0){
            $this->removeBasketModel($basket);
            throw new BasketItemDeletedException();
        }

        if($quantity > $basket->price->quantity){
            $quantity = $basket->price->quantity;
        }

        $basket->quantity = $quantity;
        $basket->save();
        return new BasketItemDTO($basket);

    }

    /**
     * @param \VmdCms\Modules\Orders\Models\Basket $basket
     * @throws BasketException
     */
    protected function removeBasketModel(\VmdCms\Modules\Orders\Models\Basket $basket)
    {
        try {
            $basket->delete();
        }catch (\Exception $exception){
            throw new BasketException();
        }
    }

    /**
     * @param int $priceId
     * @throws BasketException
     * @throws ModelNotFoundException
     */
    public function removeBasketItem(int $priceId)
    {
        $basket = $this->getBasketModel($priceId);
        if(!$basket instanceof \VmdCms\Modules\Orders\Models\Basket){
            throw new ModelNotFoundException();
        }
        $this->removeBasketModel($basket);
    }

    /**
     * @param int $priceId
     * @param int $quantity
     * @return BasketItemDTO
     */
    public function appendToBasket(int $priceId,int $quantity = 1): BasketItemDTO
    {
        $basket = $this->getBasketModel($priceId);
        if(!$basket instanceof Basket) {
            $basket = new Basket();
            $basket->user_id = auth()->guard('user')->id();;
            $basket->session_id = $this->getUserSessionId();
            $basket->price_id = $priceId;
            $basket->quantity = 0;
        }
        $basket->quantity += $quantity;
        $basket->save();

        return new BasketItemDTO($basket);
    }

    protected function getBasketItemsDTO()
    {
        $basketItems = $this->getBasketItems();
        $basketDTOArr = [];
        if(is_countable($basketItems) && !count($basketItems)) return $basketDTOArr;

        foreach ($basketItems as $item)
        {
            $basketDTOArr[] = new BasketItemDTO($item);
        }
        return $basketDTOArr;
    }

    /**
     * @param int $priceId
     * @return \VmdCms\Modules\Orders\Models\Basket|null
     */
    protected function getBasketModel(int $priceId): ?\VmdCms\Modules\Orders\Models\Basket
    {
        $userId = auth()->guard('user')->id();

        $userSessionId = $this->getUserSessionId();
        $basketQuery = Basket::where('price_id',$priceId);

        if($userId)
        {
            DB::table(Basket::table())->where('session_id',$userSessionId)->update([
                'user_id' => $userId
            ]);
            $basketQuery->where('user_id',$userId);
        }
        else{
            $basketQuery->where('session_id',$userSessionId);
        }
        return $basketQuery->first();
    }

    protected function getUserSessionId()
    {
        return UserSession::getUserSessionId();
    }

    protected function getBasketItems()
    {
        $userId = auth()->guard('user')->id();
        $query = Basket::query();
        if($userId){
            $query->where('user_id',$userId)->orWhere('session_id',$this->getUserSessionId());
        }
        else{
            $query->where('session_id',$this->getUserSessionId());
        }
        $query->with('price');
        return $query->get();
    }

    /**
     * @return BasketDTOCollectionInterface
     */
    protected function getBasketItemsDtoCollection(): BasketDTOCollectionInterface
    {
        $items = $this->getBasketItems();
        $itemsDtoCollection = new BasketDTOCollection();
        if(is_countable($items) && count($items))
        {
            foreach ($items as $item)
            {
                $itemsDtoCollection->append(new BasketItemDTO($item));
            }
        }
        return $itemsDtoCollection;
    }

    protected function getBasketTotalSum(Collection $basketItems = null)
    {
        $total = 0;
        if(!is_countable($basketItems) || !count($basketItems)) return $total;
        foreach ($basketItems as $item)
        {
            $total += $item->price->price * $item->quantity;
        }
        return $total;
    }
}
