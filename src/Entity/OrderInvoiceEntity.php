<?php

namespace VmdCms\Modules\Orders\Entity;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\Modules\NovaPoshta\Models\InternetDocument;
use VmdCms\Modules\Orders\DTO\Invoice\InvoiceOrderDTO;
use VmdCms\Modules\Orders\DTO\Invoice\InvoiceOrderItemDTO;
use VmdCms\Modules\Orders\Models\Mocks\InvoiceOrderItemMock;
use VmdCms\Modules\Orders\Models\Order;
use VmdCms\Modules\Orders\Models\OrderInvoice;
use VmdCms\Modules\Products\Models\Product;

class OrderInvoiceEntity
{
    public function getOrderItemsCollection(OrderInvoice $model): Collection
    {
        $collection = new Collection();

        $items = $model->order_items;

        if(is_countable($items) && count($items)){
            foreach ($items as $item){
                $collection->add(new InvoiceOrderItemMock($item));
            }
        }

        return $collection;
    }

    public function getOrCreateOrderInvoice(Order $order){

        $model = OrderInvoice::where('order_id',$order->id)->first();

        if(!$model instanceof OrderInvoice){
            $customer = $order->user;

            if(!$customer instanceof \VmdCms\Modules\Users\Models\User){
                throw new ModelNotFoundException();
            }

            $moderator = AuthEntity::getAuthModerator();

            $model = new OrderInvoice();

            $path = public_path('/images/logo.png');
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $model->logo_base_64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

            $model->order_id = $order->id;
            $model->moderator_id = $moderator->id;
            $model->date = Carbon::now()->format('d.m.Y');
            $model->order_number = $order->id;
            $model->order_date = Carbon::parse($order->created_at)->format('d.m.Y');
            $model->order_customer_name = $customer->fullName;
            $model->order_customer_phone = $customer->phone;

            if(class_exists(InternetDocument::class)){
                $internetDocument = InternetDocument::where('order_id',$order->id)->first();

                $deliveryCity = $internetDocument ? $internetDocument->info_sender_city_title : null;
                $deliveryWarehouse = $internetDocument ? $internetDocument->info_sender_warehouse_title : null;
                $deliveryAddress = ($deliveryCity ?? '') . ($deliveryCity && $deliveryWarehouse ? ', ' : '') . ($deliveryWarehouse ?? '');

                $model->order_delivery_address = $deliveryAddress;
                $model->order_delivery_date = $internetDocument ? $internetDocument->estimated_delivery_date : null;
                $model->order_delivery_contact_name = $internetDocument ? $internetDocument->recipient_name : null;
                $model->order_delivery_contact_phone = $internetDocument ? $internetDocument->recipients_phone : null;;
            }

            $orderItems = $order->items;

            $orderInvoiceItems = [];

            if(is_countable($orderItems)){
                $i = 1;
                foreach ($orderItems as $orderItem){
                    $product = $orderItem->product;
                    if (!$product instanceof Product) continue;

                    $currency = $order->currency->info->symbol ?? null;

                    $mockModel = (new InvoiceOrderItemMock())
                        ->setId($i++)
                        ->setTitle($product->info->title ?? null)
                        ->setQuantity($orderItem->quantity)
                        ->setPrice( $orderItem->price . ' ' . $currency)
                        ->setTotal($orderItem->total . ' ' . $currency );

                    $orderInvoiceItems[] = (new InvoiceOrderItemDTO($mockModel))->toArray();
                }
            }

            $model->order_items = $orderInvoiceItems;

            $model->order_total = $order->orderSum;
            $model->order_delivery_type = $order->deliveryType->info->title ?? null;
            $model->order_payment_type = $order->paymentType->info->title ?? null;
            $model->order_total_str = $order->orderQuantity. 'шт ' . $model->order_total;

            $model->order_responsible_manager = $moderator->fullName;
            $model->order_customer_approve = $customer->fullName;

            $model->content = view('content::admin.sections.invoice_order_html',
                ['invoiceOrderDTO' => new InvoiceOrderDTO($model)])->render();

            $model->save();
        }

        return $model;

    }

    public function getInvoiceOrderFormAdminComponent(OrderInvoice $model)
    {
        $dto = new InvoiceOrderDTO($model);
        return FormComponent::view('content::admin.sections.invoice_order_form')
            ->setData(['invoiceOrderDTO' => $dto])
            ->setStoreCallback(function (){

                $request = request();

                $model = OrderInvoice::find($request->id);
                if(!$model instanceof OrderInvoice){
                    throw new ModelNotFoundException();
                }

                $model->date = $request->get('date');
                $model->order_number = $request->get('order_number');
                $model->order_date = $request->get('order_date');
                $model->order_delivery_date = $request->get('order_delivery_date');
                $model->order_customer_name = $request->get('order_customer_name');
                $model->order_customer_phone = $request->get('order_customer_phone');
                $model->order_delivery_address = $request->get('order_delivery_address');
                $model->order_delivery_contact_name = $request->get('order_delivery_contact_name');
                $model->order_delivery_contact_phone = $request->get('order_delivery_contact_phone');
                $model->order_total = $request->get('order_total');
                $model->order_total_str = $request->get('order_total_str');
                $model->order_delivery_type = $request->get('order_delivery_type');
                $model->order_payment_type = $request->get('order_payment_type');
                $model->order_responsible_manager = $request->get('order_responsible_manager');
                $model->order_customer_approve = $request->get('order_customer_approve');

                $orderItems = [];

                $items = $request->get('order_item_number',[]);
                if(is_countable($items)){
                    foreach ($items as $index=>$number){
                        $mock = (new InvoiceOrderItemMock())
                        ->setId($number)
                        ->setTitle($request->get('order_item_title',[])[$index] ?? '')
                        ->setQuantity($request->get('order_item_quantity',[])[$index] ?? '')
                        ->setPrice($request->get('order_item_price',[])[$index] ?? '')
                        ->setTotal($request->get('order_item_total',[])[$index] ?? '');
                        $orderItems[] = (new InvoiceOrderItemDTO($mock))->toArray();
                    }
                }

                $model->order_items = $orderItems;

                $model->content = view('content::admin.sections.invoice_order_html',
                    ['invoiceOrderDTO' => new InvoiceOrderDTO($model)])->render();

                $model->save();
            });
    }
}
