<?php

namespace VmdCms\Modules\Orders\Entity;

use VmdCms\CoreCms\CoreModules\Events\Entity\Event;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\Modules\Orders\DTO\OrderParamDTO;
use VmdCms\Modules\Orders\Enums\OrderStatusEnum;
use VmdCms\Modules\Orders\Exceptions\OrderException;
use VmdCms\Modules\Orders\Models\Order;
use VmdCms\Modules\Orders\Models\OrderItem;
use VmdCms\Modules\Orders\Models\Params\DeliveryTypeParam;
use VmdCms\Modules\Orders\Models\Params\OrderStatusParam;
use VmdCms\Modules\Orders\Models\Params\PaymentTypeParam;
use VmdCms\Modules\Orders\Models\QuickOrder;
use VmdCms\Modules\Orders\Services\CoreEventEnums;
use VmdCms\Modules\Prices\DTO\PriceDTO;
use VmdCms\Modules\Prices\Entity\Currencies;
use VmdCms\Modules\Prices\Models\Price;
use VmdCms\Modules\Users\DTO\UserDTO;
use VmdCms\Modules\Users\Entity\Auth\AuthEntity;
use VmdCms\Modules\Users\Models\User;

class OrderEntity
{
    /**
     * @param int $priceId
     * @param string $phone
     * @return int
     * @throws ModelNotFoundException
     */
    public function quickBuyAction(int $priceId, string $phone): int
    {
        $price = Price::where('id',$priceId)->where('active',1)->whereHas('product',function ($q){
            $q->where('active',1);
        })->first();

        if(!$price instanceof Price){
            throw new ModelNotFoundException("Price with id $priceId not found");
        }

        try {

            $priceDTO = new PriceDTO($price);

            $currency = Currencies::getInstance()->getCurrentCurrencyDTO();
            $quickOrder = new QuickOrder();
            $quickOrder->currency_id = $currency ? $currency->getId() : null;
            $quickOrder->currency_title = $currency ? $currency->getTitle() : null;
            $quickOrder->price_id = $price->id;
            $quickOrder->product_id = $price->products_id;
            $quickOrder->phone = $phone;
            $quickOrder->price_data = json_encode($priceDTO->toArray(),JSON_UNESCAPED_UNICODE);
            $quickOrder->product_data = json_encode($priceDTO->getProductDTO()->toArray(),JSON_UNESCAPED_UNICODE);

            $quickOrder->price_base = $priceDTO->getPriceBase();
            $quickOrder->price_discount = $priceDTO->getPrice() - $priceDTO->getPriceFinal();
            $quickOrder->total = $priceDTO->getPriceFinal();

            if($user = AuthEntity::getAuthUser())
            {
                $quickOrder->user_id = $user->id;
            }

            $quickOrder->save();

        }catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }

        Event::dispatch(CoreEventEnums::USER_ORDER_QUICK_CREATE,[
            'user_id' => $user->id ?? null,
            'user_phone' => $phone,
            'price_id' => $priceId,
            'admin_link' => request()->root() . '/dashboard/quick_orders/edit/' . $quickOrder->id
        ]);

        return $quickOrder->id;
    }

    public function getUserTotalOrdersSum(User $user)
    {
        $orders = $this->getOrdersByUser($user);
        $total = 0;
        if(is_countable($orders)){
            foreach ($orders as $order){
                $total += $order->order_total;
            }
        }
        return $total . ' грн';
    }

    public function getUserTotalOrdersCount(User $user)
    {
        $orders = $this->getOrdersByUser($user);
        return is_countable($orders) ? count($orders) : 0;
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserOrdersCountByStatus(User $user): array
    {
        $countByStatuses = [];
        $orders = $this->getOrdersByUser($user);
        if(is_countable($orders)){
            $statuses = OrderStatusParam::with('info')->get();
            foreach ($orders->groupBy('status_id') as $statusId=>$order)
            {
                $statusModel = $statuses->where('id',$statusId)->first();
                $status = $statusModel->info->title ?? null;
                $countByStatuses[$status] = count($order);
            }
        }
        return $countByStatuses;
    }

    /**
     * @param User $user
     * @return int
     */
    public function getUserOrdersCountByPayment(User $user,bool $isPayed = true): int
    {
        $orders = $this->getOrdersByUser($user);
        $orders = is_countable($orders) ? $orders->where('is_payed',$isPayed) : null;

        return is_countable($orders) ? count($orders) : 0;
    }

    protected function getOrdersByUser(User $user)
    {
        return Order::where('user_id',$user->id)->with('items')->get();
    }

    /**
     * @param int $paymentTypeId
     * @param int $deliveryTypeId
     * @param string|null $comment
     * @param array $deliveryData
     * @return \App\Modules\Orders\Models\Order
     * @throws OrderException
     */
    public function createCheckoutOrder(int $paymentTypeId, int $deliveryTypeId, string $comment = null, array $deliveryData = [])
    {
        try {
            $basketInfoDTO = (new BasketEntity())->getBasketInfoDto();
            $user = AuthEntity::getAuthUser();
            $statuses = \App\Modules\Orders\Models\Params\OrderStatusParam::active()->get();

            if(!$currencyDTO = \App\Modules\Prices\Entity\Currencies::getInstance()->getCurrentCurrencyDTO()){
                throw new OrderException("Currency not set");
            }

            if(!$deliveryType = DeliveryTypeParam::where('id',$deliveryTypeId)->first()){
                throw new OrderException("Delivery type with id $deliveryTypeId not found");
            }

            if(!$paymentType = \App\Modules\Orders\Models\Params\PaymentTypeParam::where('id',$paymentTypeId)->first()){
                throw new OrderException("Payment type with id $paymentTypeId not found");
            }

            if($paymentType->slug == PaymentTypeParam::LIQPAY){
                $orderStatus = $statuses->where('slug',OrderStatusEnum::PAYMENT_WAITING)->first();
            }
            else{
                $orderStatus = $statuses->where('slug',OrderStatusEnum::NEW)->first();
            }

            if(!$orderStatus instanceof OrderStatusParam){
                throw new OrderException("Order status not available. Payment slug:" . $paymentType->slug);
            }

            $order = new Order();
            $order->user_id = $user->id;
            $order->currency_id = $currencyDTO->getId();
            $order->payment_type_id = $paymentTypeId;
            $order->delivery_type_id = $deliveryTypeId;
            $order->status_id = $orderStatus->id;
            $order->comment = $comment;
            $order->order_data = json_encode(array_merge(
                $basketInfoDTO->toArray(),[
                    'currency' => $currencyDTO->toArray(),
                    'paymentType' => (new OrderParamDTO($paymentType))->toArray(),
                    'deliveryType' => (new OrderParamDTO($deliveryType))->toArray()
                ]
            ),JSON_UNESCAPED_UNICODE);
            $order->delivery_data = json_encode($deliveryData,JSON_UNESCAPED_UNICODE);
            $order->user_data = json_encode((new UserDTO($user))->toArray(),JSON_UNESCAPED_UNICODE);
            $order->save();

            foreach ($basketInfoDTO->getBasketItems() as $item){

                $orderItem = new OrderItem();
                $orderItem->order_id = $order->id;
                $orderItem->price_id = $item->getPriceId();
                $orderItem->product_id = $item->getProductId();
                $orderItem->quantity = $item->getQuantity();
                $orderItem->price_base = $item->getPriceBase();
                $orderItem->price = $item->getPriceFinal();
                $orderItem->total = $item->getTotalSumFinal();
                $orderItem->order = isset($sort) ? $sort++ : $sort = 1;
                $orderItem->price_data =  json_encode($item->getPriceDTO()->toArray(),JSON_UNESCAPED_UNICODE);
                $orderItem->product_data = json_encode($item->getProductDTO()->toArray(),JSON_UNESCAPED_UNICODE);
                $orderItem->save();

            }

            (new OrderHistoryEntity())->storeOrderHistory($order);

            Event::dispatch(CoreEventEnums::USER_ORDER_CREATE,[
                'user_id' => $user->id,
                'user_email' => $user->email,
                'admin_link' => request()->root() . '/dashboard/orders/edit/' . $order->id
            ]);
        }
        catch (\Exception $exception){
            throw new OrderException($exception->getMessage());
        }

        return $order;
    }
}
