<?php

namespace VmdCms\Modules\Orders\Entity;

use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\DTO\Dashboard\Components\ActionHistoryDTO;
use VmdCms\Modules\Orders\Models\Order;
use VmdCms\Modules\Orders\Models\OrderHistory;

class OrderHistoryEntity
{
    const ACTION_CREATED = 'created';
    const ACTION_NOTIFICATION = 'notification';
    const ACTION_IN_WORK = 'in_work';
    const ACTION_COMMENT = 'comment';

    public function storeOrderHistory(Order $order,string $action = self::ACTION_CREATED, bool $isModerator = false)
    {
        $orderHistory = new OrderHistory();
        $orderHistory->order_id = $order->id;
        if($isModerator){
            $orderHistory->moderator_id = AuthEntity::getAuthModeratorId();
        }
        else{
            $orderHistory->user_id = AuthEntity::getAuthUserId();
        }
        $orderHistory->is_moderator = $isModerator;
        $orderHistory->action = in_array($action,[self::ACTION_CREATED,self::ACTION_NOTIFICATION,self::ACTION_IN_WORK]) ? $action : null;
        $orderHistory->value = $this->getValueByAction($order,$action,$isModerator);
        $orderHistory->save();
        return $orderHistory;
    }

    public function storeComment(Order $order,string $comment)
    {
        $orderHistory = new OrderHistory();
        $orderHistory->order_id = $order->id;
        $orderHistory->moderator_id = AuthEntity::getAuthModeratorId();
        $orderHistory->is_moderator = true;
        $orderHistory->action = self::ACTION_COMMENT;
        $orderHistory->value = $comment;
        $orderHistory->save();
        return $orderHistory;
    }

    protected function getValueByAction(Order $order,string $action, bool $isModerator = false)
    {
        $value = null;
        if($action == self::ACTION_CREATED)
        {
            if($isModerator){
                $value = 'Админ: ' . $order->moderator->full_name;
            }
            else{
                $value =  $order->user->full_name;
            }
            $value .= ' Заказ оформлен';
        }
        elseif ($action == self::ACTION_NOTIFICATION)
        {
            $value = 'Уведомление Заказ оформлен отправлено покупателю';
        }
        elseif ($action == self::ACTION_IN_WORK)
        {
            $value = ($order->moderator->full_name ?? null) . ' в обработку';
        }
        return $value;
    }

    /**
     * @param int $orderId
     * @return array
     */
    public function getActionHistoryDtoArr(int $orderId): array
    {
        $orderHistory = OrderHistory::where('order_id',$orderId)->orderBy('created_at','desc')->get();
        $orderHistoryDtoArr = [];
        if(is_countable($orderHistory) && count($orderHistory))
        {
            foreach ($orderHistory as $orderHistoryItem)
            {
                $date = date('d.m.Y', strtotime($orderHistoryItem->created_at)) . ' в ' . date('H:i:s', strtotime($orderHistoryItem->created_at));
                $icon = $orderHistoryItem->action == self::ACTION_NOTIFICATION ? 'icon-message' : null;
                $photo = $orderHistoryItem->is_moderator ? $orderHistoryItem->moderator->imagePath ?? null : $orderHistoryItem->user->imagePath ?? null;
                $title = $orderHistoryItem->value;
                $orderHistoryDtoArr[] = new ActionHistoryDTO($title,$date,$photo,$icon);
            }
        }
        return $orderHistoryDtoArr;
    }
}
