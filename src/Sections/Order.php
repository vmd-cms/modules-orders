<?php

namespace VmdCms\Modules\Orders\Sections;

use Illuminate\Support\Collection;
use VmdCms\CoreCms\Collections\DependedComponentCollection;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\FormComponentInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\Components\Input\AsyncSelectInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\CoreModules\Content\Entity\Languages;
use VmdCms\CoreCms\CoreModules\Events\Entity\Event;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventEnums;
use VmdCms\CoreCms\CoreModules\Moderators\Entity\AuthEntity;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterComponent;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterDisplay;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCustomButton;
use VmdCms\CoreCms\Dashboard\Forms\Components\ColumnComponent;
use VmdCms\CoreCms\Dashboard\Forms\Components\DependedComponent;
use VmdCms\CoreCms\Dashboard\Forms\Tables\Modals\ModalData;
use VmdCms\CoreCms\DTO\Dashboard\DependComponentDto;
use VmdCms\CoreCms\DTO\NavDTO;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Exceptions\Sections\SectionCmsModelException;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Facades\FormTableColumn;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\NovaPoshta\DTO\Admin\DataCityDTO;
use VmdCms\Modules\NovaPoshta\DTO\Admin\DataWarehouseDTO;
use VmdCms\Modules\NovaPoshta\Entity\InternetDocumentEntity;
use VmdCms\Modules\Orders\DTO\OrderDTO;
use VmdCms\Modules\Orders\Entity\OrderHistoryEntity;
use VmdCms\Modules\Orders\Entity\OrderInvoiceEntity;
use VmdCms\Modules\Orders\Models\OrderItem;
use VmdCms\Modules\Orders\Models\Params\DeliveryTypeParam;
use VmdCms\Modules\Orders\Models\Params\OrderParamInfo;
use VmdCms\Modules\Orders\Models\Params\OrderStatusParam;
use VmdCms\Modules\Orders\Models\Params\PaymentTypeParam;
use VmdCms\Modules\Prices\Entity\Currencies;
use VmdCms\Modules\Prices\Entity\PriceEntity;
use VmdCms\Modules\Prices\Models\Currency;
use VmdCms\Modules\Prices\Models\Price;
use VmdCms\Modules\Products\Models\Product;
use VmdCms\Modules\Users\DTO\UserDeliveryDTO;
use VmdCms\Modules\Users\Models\User;
use VmdCms\Modules\Users\Models\UserDelivery;

class Order extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'orders';

    public function display()
    {
        $filter = (new FilterDisplay())
            ->appendFilterComponent((new FilterComponent('info.title'))
                ->setCountCallback(function ($model) {
                    return \VmdCms\Modules\Orders\Models\Order::where('status_id', $model->id)->count();
                })
                ->setChipColorCallback(function ($model) {
                    return $model->color;
                })
                ->setHeadTitle('Статусы заказов')
                ->setExpansionTitle('Все статусы')
                ->setFilterSlug('order_status_param')
                ->setModelClass(OrderStatusParam::class)
                ->setFilterField('status_id')
            )
            ->setHeaderLink((new NavDTO('Создать Заказ','/dashboard/orders/create'))->setIcon('icon-plus'));

        return Display::dataTable([
            Column::text('id', '№ Заказа')->setWidth(100),
            Column::text('customer_name', 'Покупатель')->setSearchableCallback(function ($query, $search) {
                $query->orWhereHas('user', function ($q) use ($search) {
                    $q->where('first_name', 'like', '%' . $search . '%')
                        ->orWhere('last_name', 'like', '%' . $search . '%')
                        ->orWhere('email', 'like', '%' . $search . '%')
                        ->orWhere('phone', 'like', '%' . $search . '%');
                });
            })->setSortableCallback(function ($query, $sortType) {
                $query->join(User::table(), function ($join) {
                    $join->on(User::table() . '.id', '=', \VmdCms\Modules\Orders\Models\Order::table() . '.user_id');
                });
                $query->orderBy(User::table() . '.last_name', $sortType)
                    ->orderBy(User::table() . '.first_name', $sortType)
                    ->selectRaw('orders.*');
            }),
            Column::text('order_sum', 'Сумма заказа')->setSortableCallback(function ($query, $sortType) {
                $query->selectRaw('orders.*, sum(' . OrderItem::table() . '.total) as order_sum')
                    ->join(OrderItem::table(), function ($join) {
                        $join->on(OrderItem::table() . '.order_id', '=', \VmdCms\Modules\Orders\Models\Order::table() . '.id');
                    })
                    ->orderBy('order_sum', $sortType)->groupBy('orders.id')->selectRaw('orders.*');
            })->setWidth(140),
            Column::chipText('status.info.title', 'Статус')->setSearchableCallback(function ($query, $search) {
                $query->orWhereHas('status', function ($q) use ($search) {
                    $q->whereHas('info', function ($info) use ($search) {
                        $info->where('title', 'like', '%' . $search . '%');
                    });
                });
            })->setSortableCallback(function ($query, $sortType) {
                $query->join(OrderParamInfo::table() . ' as status_info', function ($join) {
                    $join->on('status_info.order_params_id', '=', \VmdCms\Modules\Orders\Models\Order::table() . '.status_id')
                        ->where('status_info.lang_key', '=', Languages::getInstance()->getCurrentLocale());
                });
                $query->orderBy('status_info.title', $sortType)->selectRaw('orders.*');
            })->setColorCallback(function ($model) {
                return $model->status->color ?? null;
            }),
            Column::text('paymentType.info.title', 'Способ оплаты')->setSearchableCallback(function ($query, $search) {
                $query->orWhereHas('paymentType', function ($q) use ($search) {
                    $q->whereHas('info', function ($info) use ($search) {
                        $info->where('title', 'like', '%' . $search . '%');
                    });
                });
            })->setSortableCallback(function ($query, $sortType) {
                $query->join(OrderParamInfo::table() . ' as payment_info', function ($join) {
                    $join->on('payment_info.order_params_id', '=', \VmdCms\Modules\Orders\Models\Order::table() . '.payment_type_id')
                        ->where('payment_info.lang_key', '=', Languages::getInstance()->getCurrentLocale());
                });
                $query->orderBy('payment_info.title', $sortType)->selectRaw('orders.*');
            })->setWidth(140),
            Column::text('deliveryType.info.title', 'Способ Доставки')->setSearchableCallback(function ($query, $search) {
                $query->orWhereHas('deliveryType', function ($q) use ($search) {
                    $q->whereHas('info', function ($info) use ($search) {
                        $info->where('title', 'like', '%' . $search . '%');
                    });
                });
            })->setSortableCallback(function ($query, $sortType) {
                $query->join(OrderParamInfo::table() . ' as delivery_info', function ($join) {
                    $join->on('delivery_info.order_params_id', '=', \VmdCms\Modules\Orders\Models\Order::table() . '.delivery_type_id')
                        ->where('delivery_info.lang_key', '=', Languages::getInstance()->getCurrentLocale());
                });
                $query->orderBy('delivery_info.title', $sortType)->selectRaw('orders.*');
            })->setWidth(150),
            Column::date('created_at', 'Дата и время')->setFormat('d.m.Y H:i:s')
                ->setWidth(140),
        ])->orderDefault(function ($query){
            $query->orderBy('created_at','desc');
        })->setSearchable(true)
            ->setCreateBtnTitle('Добавить Заказ')
            ->setFilter($filter);
    }

    public function isDeletable(CmsModel $model = null): bool
    {
        if(parent::isDeletable($model)){
            return request()->route()->getName() != CoreRouter::ROUTE_RESTORE ? ($model && !$model->deleted) : true;
        }
        return false;
    }

    public function onCreated(CmsModelInterface $model): void
    {
        parent::onCreated($model);
        if($model instanceof \VmdCms\Modules\Orders\Models\Order)
        {
            (new OrderHistoryEntity())->storeOrderHistory($model,OrderHistoryEntity::ACTION_CREATED,true);
        }
    }

    /**
     * @return FormInterface
     */
    public function create(): FormInterface
    {
        $this->title = 'Создание Заказа';

        return Form::panel([
            FormComponent::input('moderator_id')->addClass('d-none','container')
                ->setStoreCallback(function ($model){
                    $model->moderator_id = AuthEntity::getAuthModeratorId();
                }),
            FormComponent::select('status_id', 'Статус заказа')
                ->setModelForOptions(OrderStatusParam::class)
                ->setDisplayField('info.title')
                ->addClass('pt-10')
                ->required(),
            FormComponent::select('currency_id', 'Валюта заказа')
                ->setModelForOptions(Currency::class)
                ->setDisplayField('info.symbol')
                ->required(),
            $this->getUserAsyncSelectComponent()->required(),
            FormComponent::row('')
                ->appendColumn((new ColumnComponent(6))->appendComponents([
                    FormComponent::select('payment_type_id', 'Оплата')
                        ->setModelForOptions(PaymentTypeParam::class)
                        ->setDisplayField('info.title')
                        ->displayRow()->addClass('pb-5')
                        ->required(),
                ]))
                ->appendColumn((new ColumnComponent(6))->appendComponents([
                    $this->getDeliveryComponent()->required(),
                ])),
        ]);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id): FormInterface
    {
        $model = $this->getCmsModel()::find($id);

        $this->title = 'Редактирование заказа №' . $id;

        $headPanelRow = FormComponent::row('')
            ->appendColumn((new ColumnComponent(9))->appendComponents([
                FormComponent::html('<span class="light">' . date('d.m.Y', strtotime($model->created_at)) . ' в ' . date('H:i:s', strtotime($model->created_at)) . ' </span>')
                    ->setPaddingTop(10),
            ]))
            ->appendColumn((new ColumnComponent(3))->appendComponents([
                FormComponent::select('status_id', '')
                    ->setModelForOptions(OrderStatusParam::class)
                    ->setDisplayField('info.title')
                    ->justify('end')
                    ->addClass('ml-3 mt-3 max-width-200px', 'component_container')
                    ->setShowLabel(false)->hideDetails(),
            ]));

        $columnOrderInfoComponents = [
            FormComponent::html('<span class="fs-20px fw-400">Заказ № ' . $model->id . ' </span>')->addClass('pb-5'),
            FormComponent::html('<span class="light">Ip-адрес:</span><span>192.168.0.0</span>')->addClass('pb-5'),
            FormComponent::text('comment', 'Коментарии к заказу')->displayRow()->addClass('width-330px textarea-h-60px'),

            FormComponent::row('')
                ->appendColumn((new ColumnComponent(6))->appendComponents([
                    FormComponent::select('payment_type_id', 'Оплата')
                        ->setModelForOptions(PaymentTypeParam::class)
                        ->setDisplayField('info.title')
                        ->displayRow()->addClass('pb-5'),
                ]))
                ->appendColumn((new ColumnComponent(6))->appendComponents([
                    $this->getDeliveryComponent($model),
                ])),
        ];

        $columnButtonsComponents = [
            FormComponent::buttons('')->appendButtons(
                (new FormCustomButton('Cохранить'))
                    ->setAction(FormButton::ACTION_SAVE)
                    ->setIconClass('icon-edit')),
        ];

        if($model instanceof \VmdCms\Modules\Orders\Models\Order) {
            $columnButtonsComponents[] = FormComponent::buttons('')->appendButtons((new FormCustomButton( 'Накладная'))
                ->setRoutePath(route(CoreRouter::ROUTE_SERVICE_DATA,[
                    'sectionSlug' => 'orders',
                    'sectionMethod' => 'createOrderInvoiceAdminSectionLink',
                    'id' => $model->id
                ]))
                ->setConfirmMessage('Сохраните изменения перед созданием Накладной. </br>Продолжить?')
                ->setAction(FormButton::ACTION_SERVICE_PATH)
                ->setIconClass('icon-print'));
        }

        if($model instanceof \VmdCms\Modules\Orders\Models\Order &&
            class_exists(\VmdCms\Modules\NovaPoshta\Entity\InternetDocumentEntity::class))
        {
            $columnButtonsComponents[] = FormComponent::buttons('')->appendButtons((new FormCustomButton( 'ТТН'))
                ->setRoutePath(route(CoreRouter::ROUTE_SERVICE_DATA,[
                    'sectionSlug' => 'orders',
                    'sectionMethod' => 'createInternetDocumentAdminSectionLink',
                    'id' => $model->id
                ]))
                ->setConfirmMessage('Сохраните изменения перед созданием Экспресс Накладной (ЭН). </br>Продолжить?')
                ->setAction(FormButton::ACTION_SERVICE_PATH)
                ->setIconClass('icon-calendar'));
        }

        if($model->deleted){
            $columnButtonsComponents[] =  FormComponent::buttons('')->appendButtons(
                (new FormCustomButton('Восстановить'))
                    ->setAction(FormButton::ACTION_RESTORE)
                    ->setRoutePath(CoreRouter::getRestoreRoute($this->getSectionSlug(),$id))
                    ->setIconClass('icon-off'));
        }else{
            $columnButtonsComponents[] =  FormComponent::buttons('')->appendButtons(
                (new FormCustomButton('Удалить'))
                    ->setAction(FormButton::ACTION_DELETE)
                    ->setRoutePath(CoreRouter::getDeleteRoute($this->getSectionSlug(),$id))
                    ->setIconClass('icon-delete'));
        }

        $columnOrderComponents = [
            $headPanelRow,
            FormComponent::row('')
                ->appendColumn((new ColumnComponent(9))->appendComponents($columnOrderInfoComponents)->addClass('pt-0'))
                ->appendColumn((new ColumnComponent(3))->appendComponents($columnButtonsComponents)),
        ];

        $columnProfilePreviewComponents = [
            FormComponent::view('content::admin.sections.profile_preview')
                ->setData(['model' => $model->user])->wide(),
            FormComponent::html($this->getUserProfileLink($model)),
        ];

        $orderHistory = FormComponent::actionHistory('order_history_comment', 'История заказа')
            ->setAppendTitle('Добавить комментарий')
            ->appendItems((new OrderHistoryEntity())->getActionHistoryDtoArr($model->id))
            ->setStoreCallback(function ($model){
                if($model instanceof \VmdCms\Modules\Orders\Models\Order && $message = request()->get('order_history_comment'))
                    return (new OrderHistoryEntity())->storeComment($model,$message);
            })
            ->displayRow()
            ->wide()
            ->addClass('px-7 pb-10');

        return Form::panel([
            FormComponent::row('')
                ->appendColumn(
                    (new ColumnComponent(9))->appendComponents($columnOrderComponents)->addClass('pt-0')
                )->appendColumn(
                    (new ColumnComponent(3))->appendComponents($columnProfilePreviewComponents)->borderLeft()
                ),
            $this->getOrderItemsFormComponent($model),
            $orderHistory,
        ])->hideFloatActionGroup()->hideFooterActionGroup()->wide();
    }

    public function refreshModalData()
    {
        $orderId = request()->id ?? null;
        if(empty($orderId)){
            throw new SectionCmsModelException('Empty order id');
        }

        $order = \VmdCms\Modules\Orders\Models\Order::where('id',$orderId)->with('items')->first();
        if(!$order instanceof \VmdCms\Modules\Orders\Models\Order){
            throw new ModelNotFoundException('Model not found');
        }

        $user = $order->user;
        if(!$user instanceof User){
            throw new ModelNotFoundException();
        }

        $orderItems = $order->items;

        $this->modifyOrderItems($orderItems,$order);

        $orderItem = $this->findOrCreateOrderItem($orderItems,$orderId,request()->get('model_id'));

        $this->updateOrderItem($orderItem,request()->toArray(),$user);

        $orderItemsComponent = $this->getOrderItemsFormComponent($order)->setCmsModel($order)->toArray();
        return json_encode(['table_data' => $orderItemsComponent['tableData']]);
    }

    public function searchUserAsync(){
        $component = $this->getUserAsyncSelectComponent();
        $component->setOffset( request()->get('offset',0))->setSearch(request()->get('search'));
        return json_encode($component->toArray());
    }

    public function searchProductPriceAsync(){
        $orderId = request()->id ?? null;

        if(empty($orderId)){
            throw new SectionCmsModelException('Empty order id');
        }

        $order = \VmdCms\Modules\Orders\Models\Order::where('id',$orderId)->with('items')->first();
        if(!$order instanceof \VmdCms\Modules\Orders\Models\Order){
            throw new ModelNotFoundException('Model not found');
        }

        $orderItemId = request()->model_id ?? null;
        $orderItemModel = $orderItemId ? OrderItem::find($orderItemId) : null;

        $productPriceComponent = $this->getProductPriceAsyncSelectComponent($order);

        if($orderItemModel instanceof OrderItem){
            $productPriceComponent->setCmsModel($orderItemModel);
        }

        $productPriceComponent->setOffset( request()->get('offset',0))->setSearch(request()->get('search'));
        return json_encode($productPriceComponent->toArray());
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Orders\Models\Order::class;
    }

    /**
     * @return AsyncSelectInterface
     */
    protected function getProductPriceAsyncSelectComponent(\VmdCms\Modules\Orders\Models\Order $model = null): AsyncSelectInterface
    {
        return FormComponent::asyncSelect('price_id','Товар/Цена')
            ->setModelForOptions(Price::class)
            ->setModifyQueryCallback(function ($query,$model){
//                if($model instanceof OrderItem){
//                    $query->where('products_id',$model->product_id);
//                }
            })
            ->setDisplayTitleCallback(function ($model){
                $currency = Currencies::getInstance()->getCurrentCurrencySymbol();
                $filtersInfo = [];
                if(is_countable($model->filters)){
                    foreach ($model->filters as $filter){
                        $filterGroupTitle = $filter->parent->info->title ?? null;
                        $filterTitle = $filter->info->title ?? null;
                        $filtersInfo[] = $filterGroupTitle . ':' . $filterTitle;
                    }
                }
                $productTitle = $model->product->info->title ?? null;
                return $productTitle . ' - ' . $model->price . $currency . ' (' . implode(',',$filtersInfo) . ')';
            })
            ->setSearchCallback(function ($query,$search){
                if(empty($search)) return;
                $query->where('active',true)->where('quantity','>',0)
                    ->whereHas('product',function ($hasQuery) use ($search){
                        $hasQuery->where('code','like','%' . $search . '%')
                            ->orWhereHas('info',function ($q) use($search){
                                $q->where('title','like','%' . $search . '%')
                                    ->orWhere('url','like','%' . $search . '%');
                            });
                    });
            })
            ->setLimit(10)
            ->setAsyncMethodPath(route(CoreRouter::ROUTE_SERVICE_DATA,
                [
                    'sectionSlug' => $this->slug,
                    'id' => $model->id,
                    'sectionMethod'=>'searchProductPriceAsync'
                ],false));
    }

    protected function getOrderItemsFormComponent(\VmdCms\Modules\Orders\Models\Order $model): FormComponentInterface
    {
        $editModal = new ModalData();
        $editModal->setServiceMethodPath(route(CoreRouter::ROUTE_SERVICE_DATA,
            [
                'sectionSlug' => $this->slug,
                'id' => $model->id,
                'sectionMethod'=>'refreshModalData'
            ],false));
        $editModal->setComponents([
            $this->getProductPriceAsyncSelectComponent($model)->setRows(3,9),
            FormComponent::input('quantity','Количество')
                ->setRows(3,9)
                ->addClass('pt-5')
                ->addClass('max-width-100px','component')
                ->integer()->min(1),
        ]);

        $orderItems = FormComponent::datatable('items', 'Товары')
            ->setComponents([
                FormTableColumn::custom('price_id', 'Название')->setContentCallback(function ($orderItemModel) {
                    return $this->getOrderItemProductPreview($orderItemModel);
                })->alignStart(),
                FormTableColumn::custom('quantity', 'Кол-во')->setContentCallback(function ($orderItemModel) {
                    return $this->getOrderItemProductQuantity($orderItemModel);
                })->alignStart(),
                FormTableColumn::custom('total', 'Итого')->setContentCallback(function ($orderItemModel) {
                    return $this->getOrderItemProductTotal($orderItemModel);
                })->alignStart(),
            ])
            ->setIsOrderable(false)
            ->setIsEditable(true)
            ->setIsCreatable(true)
            ->setEditModal($editModal)
            ->displayRow()
            ->setInfo($this->getOrderItemsInfo($model))
            ->setStoreCallback(function ($model){

                $orderItems = $model->items;
                $updateOrderItems = $this->modifyOrderItems($orderItems,$model);
                $changes = [];

                if(is_countable($updateOrderItems)){
                    foreach ($updateOrderItems as $item){
                        unset($item->updatedComponent);
                        if(intval($item->id) <= 0) {
                            $item->id = null;
                        }
                        $changes['updated'][] = [
                            'id' => $item->id,
                            'order_id' => $item->order_id,
                            'price_id' => $item->price_id,
                            'product_id' => $item->product_id,
                            'quantity' => $item->quantity,
                            'price_base' => $item->price_base,
                            'price' => $item->price,
                            'total' => $item->total,
                        ];
                        $item->save();
                    }
                }

                $deletedItems = request()->get('deleted_items');
                if (is_array($deletedItems) && count($deletedItems)){
                    $items =  OrderItem::where('order_id',$model->id)->whereIn('id',$deletedItems)->get();
                    if(is_countable($items)){
                        foreach ($items as $item){
                            $changes['deleted'][] = [
                                'id' => $item->id,
                                'order_id' => $item->order_id,
                                'price_id' => $item->price_id,
                                'product_id' => $item->product_id,
                                'quantity' => $item->quantity,
                                'price_base' => $item->price_base,
                                'price' => $item->price,
                                'total' => $item->total,
                            ];
                            $item->delete();
                        }
                    }
                }

                if(is_countable($changes) && count($changes)){
                    Event::dispatch(CoreEventEnums::ADMIN_EDIT,$changes);
                }

            })
            ->addClass('px-7');

        return $orderItems;
    }

    /**
     * @param Collection $orderItems
     * @param Order $order
     * @return array
     */
    protected function modifyOrderItems(Collection $orderItems,\VmdCms\Modules\Orders\Models\Order $order): array{
        $requestArr = request()->toArray();

        $orderId = $order->id;

        $user = $order->user;

        $updateOrderItems = [];
        if(isset($requestArr['items_quantity']) && is_countable($requestArr['items_quantity'])){
            foreach ($requestArr['items_quantity'] as $orderItemId => $value){
                if(empty($value) || intval($value) <= 0) continue;
                $orderItem = $this->findOrCreateOrderItem($orderItems,$orderId,$orderItemId);
                $this->updateOrderItem($orderItem,['quantity' => $value],$user);
                $updateOrderItems[$orderItem->id] = $orderItem;
            }
        }
        if(isset($requestArr['items_price_id']) && is_countable($requestArr['items_price_id'])){
            foreach ($requestArr['items_price_id'] as $orderItemId => $value){
                if(empty($value) || intval($value) <= 0) continue;
                $orderItem = $this->findOrCreateOrderItem($orderItems,$orderId,$orderItemId);
                $this->updateOrderItem($orderItem,['price_id' => $value],$user);
                $updateOrderItems[$orderItem->id] = $orderItem;
            }
        }
        return $updateOrderItems;
    }

    protected function findOrCreateOrderItem(Collection $orderItems, int $orderId, $orderItemId = null){
        $orderItem = $orderItems->where('id',$orderItemId)->first();
        if(!$orderItem instanceof OrderItem){
            $orderItem = new OrderItem();
            $orderItem->id = null;
            $orderItem->order_id = $orderId;
            $orderItem->order = 1;
            $orderItems->push($orderItem);
        }
        return $orderItem;
    }

    protected function updateOrderItem(OrderItem $orderItem,$params = [], User $user = null)
    {
        $quantity = $params['quantity'] ?? null;
        if($quantity && intval($quantity) > 0){
            $orderItem->quantity = $quantity;
            $orderItem->total = $orderItem->price * $quantity;
            //   $orderItem->updatedComponent = true;
        }

        $priceId = $params['price_id'] ?? null;
        if($priceId && intval($priceId) > 0 && $price = Price::where('id',$priceId)->first()){
            $orderItem->product_id = $price->products_id;
            $orderItem->price_id = $priceId;
            $orderItem->price_base = $price->price;
            $orderItem->price = PriceEntity::getPriceWithActionDiscountsFinal($price,$user);
            $orderItem->total = $orderItem->price * $orderItem->quantity;
            //   $orderItem->updatedComponent = true;
        }

        $orderItem->save();
    }

    protected function getOrderItemProductPreview(OrderItem $model)
    {
        $product = $model->product;
        if (!$product instanceof Product) return null;

        $productLink = route(CoreRouter::ROUTE_EDIT_GET,['sectionSlug' => 'products', 'id' => $product->id],false);

        return '<a class="column-preview-block" href="'.$productLink.'" target="_blank">
                        <div class="photo-preview"><img src="' . $product->imagePath . '"></div>
                        <div class="pl-1">
                            <div class="text-left pb-1"><span class="title-text">' . ($product->info->title ?? null) . '</span></div>
                            <div class="text-left">
                                <span>Бренд: </span>
                                <span>' . ($product->brand->info->title ?? null) . '</span>
                            </div>
                        </div>
                    </a>';
    }

    protected function getOrderItemProductSupplier(OrderItem $model)
    {
        $product = $model->product;
        if (!$product instanceof Product) return null;

        return '<div class="column-preview-block">
                    <div class="text-left"><span>' . ($product->supplier->info->title ?? null) . '</span></div>
                </div>';
    }

    protected function getOrderItemProductQuantity(OrderItem $model)
    {
        $currency = $model->orderModel->currency->info->symbol ?? null;
        return '<div class="column-preview-block">
                    <div class="text-left">
                        <span>' . $model->price . '&nbsp' . $currency . '</span>
                        <span>&nbspx&nbsp</span>
                        <span>' . $model->quantity . '</span>
                    </div>
                </div>';
    }

    protected function getOrderItemProductTotal(OrderItem $model)
    {
        $currency = $model->orderModel->currency->info->symbol ?? null;
        return '<div class="column-preview-block">
                    <div class="text-left">
                        <span>' . $model->total . '&nbsp' . $currency . '</span>
                    </div>
                </div>';
    }

    protected function getOrderItemsInfo(\VmdCms\Modules\Orders\Models\Order $model)
    {
        $orderDto = new OrderDTO($model);
        $currency = $model->currency->info->symbol ?? null;
        return '<div class="d-flex align-end flex-column">
                    <div class="width-100px  pt-3 d-flex justify-space-between">
                        <span>Скидка:</span>
                        <span>' . $orderDto->getTotalDiscountSum() . ' ' . $currency . '</span>
                    </div>
                    <div class="width-100px bold pt-3 pb-10 d-flex justify-space-between">
                        <span>Итого:</span>
                        <span>' . $orderDto->getTotalSum() . ' ' . $currency . '</span>
                    </div>
                </div>';
    }

    protected function getUserProfileLink(\VmdCms\Modules\Orders\Models\Order $order)
    {
        $link = route(CoreRouter::ROUTE_EDIT_GET,['sectionSlug' => 'users','id' => $order->user_id],false);
        return '<div><a class="link-button-component" href="'.$link.'">Посмотреть профиль</a></div>';
    }

    protected function getDeliveryComponent(\VmdCms\Modules\Orders\Models\Order $order = null)
    {
        $orderParamDelivery = DeliveryTypeParam::where('slug','novaposhta')->first();
        $orderParamDeliveryId = $orderParamDelivery instanceof DeliveryTypeParam ? $orderParamDelivery->id : null;

        if(empty($orderParamDeliveryId)){
            return FormComponent::select('deliveryType', 'Доставка')
                ->setDisplayField('info.title')
                ->displayRow()->addClass('pb-5');
        }

        $delivery = !empty($order) && !empty($order->delivery_data) ? json_decode($order->delivery_data,true) : null;
        $userDelivery = null;
        if(is_array($delivery)){
            $userDelivery = new UserDelivery();
            $userDelivery->order_param_id = isset($delivery['delivery_type']) ? intval($delivery['delivery_type']) : null;
            $userDelivery->city_title = isset($delivery['delivery_city_title']) ? $delivery['delivery_city_title'] : null;
            $userDelivery->city_ref = isset($delivery['delivery_city_ref']) ? $delivery['delivery_city_ref'] : null;
            $userDelivery->city_delivery_id = isset($delivery['delivery_city_id']) ? $delivery['delivery_city_id'] : null;
            $userDelivery->warehouse_title = isset($delivery['delivery_warehouse_title']) ? $delivery['delivery_warehouse_title'] : null;
            $userDelivery->warehouse_ref = isset($delivery['delivery_warehouse_ref']) ? $delivery['delivery_warehouse_ref'] : null;
        }
        $userDeliveryDTO = $userDelivery instanceof UserDelivery ? new UserDeliveryDTO($userDelivery) : null;

        $dataCityDTO = new DataCityDTO(
            $userDeliveryDTO ? $userDeliveryDTO->getCityTitle() : null,
            $userDeliveryDTO ? $userDeliveryDTO->getCityDeliveryId() : null,
            $userDeliveryDTO ? $userDeliveryDTO->getCityRef() : null,
        );

        $dataWarehouseDTO = new DataWarehouseDTO(
            $userDeliveryDTO ? $userDeliveryDTO->getWarehouseTitle() : null,
            $userDeliveryDTO ? $userDeliveryDTO->getWarehouseRef() : null,
        );

        $dependedCollection = new DependedComponentCollection();
        $dependedCollection->appendItem(new DependComponentDto(
            $orderParamDeliveryId,
            FormComponent::view('')
                ->setViewPath('vmd_cms::admin.services.novaposhta.wrapper')
                ->setData([
                    'dataCityDTO' => $dataCityDTO,
                    'dataWarehouseDTO' => $dataWarehouseDTO
                ])
                ->setStoreCallback(function ($model) use($orderParamDeliveryId){
                    $selectedDeliveryId = request()->get('delivery_type_id') ? intval(request()->get('delivery_type_id')) : null;
                    $deliveryData = null;
                    if($selectedDeliveryId == $orderParamDeliveryId)
                    {
                        $deliveryData['delivery_type'] = $selectedDeliveryId;
                        $deliveryData['delivery_city_title'] = request()->get('delivery_city_title');
                        $deliveryData['delivery_city_ref'] = request()->get('delivery_city_ref');
                        $deliveryData['delivery_city_id'] = request()->get('delivery_city_id');
                        $deliveryData['delivery_warehouse_title'] = request()->get('delivery_warehouse_title');
                        $deliveryData['delivery_warehouse_ref'] = request()->get('delivery_warehouse_ref');
                        $deliveryData = json_encode($deliveryData,JSON_UNESCAPED_UNICODE);
                    }
                    if($model->delivery_data != $deliveryData){
                        Event::dispatch(CoreEventEnums::ADMIN_EDIT,[
                            'delivery_data' => $model->delivery_data,
                            'updated_delivery_data' => $deliveryData,
                        ]);
                    }
                    $model->delivery_data = $deliveryData;
                })
        ));

        return FormComponent::dependedSelect('delivery_type_id', 'Доставка')
            ->setModelForOptions(DeliveryTypeParam::class)
            ->setDisplayField('info.title')
            ->setDisplayField('info.title')
            ->setDependedComponent(new DependedComponent('',$dependedCollection))
            ->displayRow()->addClass('pb-5');

    }

    protected function getUserAsyncSelectComponent(){
        return FormComponent::asyncSelect('user_id','Клиент')
            ->setModelForOptions(User::class)
            ->setDisplayTitleCallback(function ($model){
                return $model->full_name . ' ' . $model->email;
            })
            ->setSearchCallback(function ($query,$search){
                if(empty($search)) return;
                $query->where('first_name','like','%' . $search . '%')
                    ->orWhere('last_name','like','%' . $search . '%')
                    ->orWhere('phone','like','%' . $search . '%')
                    ->orWhere('email','like','%' . $search . '%');
            })
            ->setLimit(10)
            ->setAsyncMethodPath(route(CoreRouter::ROUTE_SERVICE_DATA,
                [
                    'sectionSlug' => $this->slug,
                    'sectionMethod'=>'searchUserAsync'
                ],false));
    }

    public function createInternetDocumentAdminSectionLink(){

        try {
            $model = \VmdCms\Modules\Orders\Models\Order::find(request()->id);
            if(!$model instanceof \VmdCms\Modules\Orders\Models\Order){
                throw new ModelNotFoundException();
            }
            $link = (new InternetDocumentEntity())->getCreateInternetDocumentAdminSectionLink($model);

        }catch (\Exception $exception){
            return ApiResponse::error('Ошибка создания накладной');
        }

        return ApiResponse::success([
            'location_open' => $link,
            'message' => ''
        ]);
    }

    public function createOrderInvoiceAdminSectionLink()
    {
        try {
            $order = \VmdCms\Modules\Orders\Models\Order::find(request()->id);
            if(!$order instanceof \VmdCms\Modules\Orders\Models\Order){
                throw new ModelNotFoundException();
            }
            $model = (new OrderInvoiceEntity())->getOrCreateOrderInvoice($order);

            $link = CoreRouter::getEditRoute((new \VmdCms\Modules\Orders\Sections\OrderInvoice())->getSectionSlug(),$model->id);


        }catch (\Exception $exception){

            return ApiResponse::error('Ошибка создания накладной');
        }

        return ApiResponse::success([
            'location_open' => $link,
            'message' => ''
        ]);
    }


}
