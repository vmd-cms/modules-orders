<?php

namespace VmdCms\Modules\Orders\Sections\Components;

use VmdCms\CoreCms\CoreModules\Translates\Sections\AbstractTranslate;

class OrderTranslate extends AbstractTranslate
{
    /**
     * @var string
     */
    protected $slug = 'Order_translates';

    public function getCmsModelClass(): string
    {
        return \App\Modules\Orders\Models\Components\OrderTranslate::class;
    }
}
