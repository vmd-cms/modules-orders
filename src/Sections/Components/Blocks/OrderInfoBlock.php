<?php

namespace VmdCms\Modules\Orders\Sections\Components\Blocks;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\Modules\Orders\Sections\Components\OrderGroupBlock;

class OrderInfoBlock extends OrderGroupBlock
{
    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('key','Ключ')->setDisabled(true),
            FormComponent::switch('active','Active'),
            FormComponent::input('description','Описание'),
            FormComponent::input('title','Title'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Orders\Models\Components\Blocks\OrderInfoBlock::class;
    }

}

