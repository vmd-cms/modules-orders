<?php

namespace VmdCms\Modules\Orders\Sections;

use Dompdf\Dompdf;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Models\CloneableInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormBackButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCancelButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCopyButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCreateButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormCustomButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormSaveButton;
use VmdCms\CoreCms\Dashboard\Forms\Buttons\FormSaveCreateButton;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\CoreCms\Services\CoreRouter;
use VmdCms\CoreCms\Services\Responses\ApiResponse;
use VmdCms\Modules\Orders\Entity\OrderInvoiceEntity;

class OrderInvoice extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'order_invoices';

    protected $title = 'Invoice';


    public function edit(?int $id): FormInterface
    {
        $model = \VmdCms\Modules\Orders\Models\OrderInvoice::find($id);
        if(!$model instanceof \VmdCms\Modules\Orders\Models\OrderInvoice){
            throw new ModelNotFoundException();
        }
        $this->setTitle('Инвойс к заказу №' . $model->order_id);

        return Form::panel([
            (new OrderInvoiceEntity())->getInvoiceOrderFormAdminComponent($model)
        ])->hideFloatActionGroup()->wide();
    }

    protected function getEditButtons(AdminSectionInterface $section, CmsModel $model): array
    {
        $this->appendFormButton(new FormSaveButton());
        $this->appendFormButton((new FormCustomButton('Скачать'))
            ->setRoutePath(route(CoreRouter::ROUTE_SERVICE_DATA,[
                'sectionSlug' => $this->getSectionSlug(),
                'sectionMethod' => 'downloadInvoice',
                'id' => $model->id
            ]))
            ->setGroupKey(FormButton::GROUP_ACTION_FOOTER)
            ->setAction(FormButton::ACTION_SERVICE_PATH)
            ->setIconClass('icon-descargar'));

        return $this->getFormButtons();
    }

    public function downloadInvoice(){

        try {
            $invoice = \VmdCms\Modules\Orders\Models\OrderInvoice::find(request()->id);

            if(!$invoice instanceof \VmdCms\Modules\Orders\Models\OrderInvoice){
                throw new ModelNotFoundException();
            }

            $filename = 'invoice№' . $invoice->order_id . '.pdf';

            $domPDF = new Dompdf();

            $html = "<html><body>" . $invoice->content . "</body></html>";

            $domPDF->loadHtml("<html><body>" . $invoice->content . "</body></html>");
            $domPDF->render();

            $content = base64_encode($domPDF->output());


        }catch (\Exception $exception){

            return ApiResponse::error('Ошибка скачивания накладной');
        }

        return ApiResponse::success([
            'download_file_content' => $content,
            'download_file_name' => $filename,
            'message' => ''
        ]);

    }



    /*
    public function edit(?int $id): FormInterface
    {
        $model = \VmdCms\Modules\Orders\Models\OrderInvoice::find($id);

        if(!$model instanceof \VmdCms\Modules\Orders\Models\OrderInvoice){
            throw new ModelNotFoundException();
        }

        $order = $model->order;

        if(!$order instanceof \VmdCms\Modules\Orders\Models\Order){
            throw new ModelNotFoundException();
        }

        $customer = $order->user;

        if(!$customer instanceof \VmdCms\Modules\Users\Models\User){
            throw new ModelNotFoundException();
        }

        if(class_exists(\VmdCms\Modules\NovaPoshta\Models\InternetDocument::class)){
            $internetDocument = \VmdCms\Modules\NovaPoshta\Models\InternetDocument::where('order_id',$order->id)->first();

            $deliveryDate = $internetDocument ? $internetDocument->estimated_delivery_date : null;
            $deliveryContact = $internetDocument ? $internetDocument->recipient_name : null;
            $deliveryContactPhones = $internetDocument ? $internetDocument->recipients_phone : null;
            $deliveryCity = $internetDocument ? $internetDocument->info_sender_city_title : null;
            $deliveryWarehouse = $internetDocument ? $internetDocument->info_sender_warehouse_title : null;
            $deliveryAddress = ($deliveryCity ?? '') . ($deliveryCity && $deliveryWarehouse ? ', ' : '') . ($deliveryWarehouse ?? '');
        }

        $headPanelRow = FormComponent::row('')
            ->appendColumn((new ColumnComponent(8))->appendComponents([
                FormComponent::html('<div>Header Logo</div>')
                    ->setPaddingTop(10),
            ]))
            ->appendColumn((new ColumnComponent(4))->appendComponents([
                FormComponent::input('date', 'Дата')->setDefault(Carbon::now()->format('d.m.Y')),
                FormComponent::input('order_number', 'Заказ №')->setDefault($order->id),
                FormComponent::input('order_date', 'от')->setDefault(Carbon::parse($order->created_at)->format('d.m.Y')),
                FormComponent::input('order_delivery_date', 'Дата Доставки')->setDefault($deliveryDate ?? null),
            ]));

        $customerRow = FormComponent::row('')
            ->appendColumn((new ColumnComponent(12))->appendComponents([
                FormComponent::input('order_customer_name', 'Покупатель')->setDefault($customer->fullName),
                FormComponent::input('order_customer_name', 'Тел покупателя')->setDefault($customer->phone),
                FormComponent::input('order_delivery_address', 'Адрес доставки')->setDefault($deliveryAddress ?? null),
                FormComponent::input('order_delivery_contact_name', 'Получатель')->setDefault($deliveryContact ?? null),
                FormComponent::input('order_delivery_contact_phone', 'Тел Получателя')->setDefault($deliveryContactPhones ?? null),
            ]));

        $orderItems = FormComponent::datatable('order_items_list','')
            ->setComponents([
                FormTableColumn::input('title','Название')->setHideDetails(true),
                FormTableColumn::input('quantity','шт.')->setWidth(75)->alignCenter()->setHideDetails(true),
                FormTableColumn::input('price','цена')->setWidth(125)->alignCenter()->setHideDetails(true),
                FormTableColumn::input('total','всего')->setWidth(125)->alignCenter()->setHideDetails(true),
            ])
            ->setStoreCallback(function ($model){
                $request = request();
                $orderItems = [];
                $orderItemIds = $request->get('order_items_list_id',[]);
                $orderItemTitles = $request->get('order_items_list_title',[]);
                $orderItemQuantities = $request->get('order_items_list_quantity',[]);
                $orderItemPrices = $request->get('order_items_list_price',[]);
                $orderItemTotals = $request->get('order_items_list_total',[]);
                if(is_countable($orderItemIds)){
                    for ($i = 0; $i < count($orderItemIds); $i++){
                        $item = (new InvoiceOrderItemMock())
                            ->setId($i+1)
                            ->setTitle($orderItemTitles[$i] ?? null)
                            ->setQuantity($orderItemQuantities[$i] ?? null)
                            ->setPrice($orderItemPrices[$i] ?? null)
                            ->setTotal($orderItemTotals[$i] ?? null);
                        $orderItems[] = $item->toArray();
                    }
                }
                $model->order_items = $orderItems;
                $model->save();
            })
            ->setPaddingBottom(50)
            ->displayRow();

        $totalRow = FormComponent::row('')
            ->appendColumn((new ColumnComponent(12))->appendComponents([
                FormComponent::input('order_total_str', 'Итого'),
                FormComponent::input('order_delivery_type', 'Доставка'),
                FormComponent::input('order_payment_type', 'Оплата'),
                FormComponent::input('order_total', 'Сумма'),
            ]));
        return Form::panel([
            $headPanelRow,
            $customerRow,
            $orderItems,
            $totalRow
        ])->hideFloatActionGroup()->wide();
    }

    */

    public function getCmsModelClass(): string
    {
        return \VmdCms\Modules\Orders\Models\OrderInvoice::class;
    }

    public function isCreatable(): bool
    {
        return false;
    }

    public function isDisplayable(): bool
    {
        return false;
    }

}
