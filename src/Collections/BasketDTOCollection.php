<?php

namespace VmdCms\Modules\Orders\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\Orders\Contracts\BasketDTOCollectionInterface;
use VmdCms\Modules\Orders\Contracts\BasketDTOInterface;

class BasketDTOCollection extends CoreCollectionAbstract implements BasketDTOCollectionInterface
{
    /**
     * @param BasketDTOInterface $dto
     */
    public function append(BasketDTOInterface $dto)
    {
        $this->collection->add($dto);
    }

    public function toArray()
    {
        $itemsArr = [];
        foreach ($this->collection as $item){
            $itemsArr[] = $item->toArray();
        }
        return $itemsArr;
    }
}
