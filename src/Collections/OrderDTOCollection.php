<?php

namespace VmdCms\Modules\Orders\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\Orders\Contracts\OrderDTOCollectionInterface;
use VmdCms\Modules\Orders\Contracts\OrderDTOInterface;

class OrderDTOCollection extends CoreCollectionAbstract implements OrderDTOCollectionInterface
{
    /**
     * @param OrderDTOInterface $dto
     */
    public function append(OrderDTOInterface $dto)
    {
        $this->collection->add($dto);
    }
}
