<?php

namespace VmdCms\Modules\Orders\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\Orders\Contracts\OrderItemDTOCollectionInterface;
use VmdCms\Modules\Orders\Contracts\OrderItemDTOInterface;

class OrderItemDTOCollection extends CoreCollectionAbstract implements OrderItemDTOCollectionInterface
{
    /**
     * @param OrderItemDTOInterface $dto
     */
    public function append(OrderItemDTOInterface $dto)
    {
        $this->collection->add($dto);
    }

    public function toArray()
    {
        $arr = [];
        foreach ($this->collection as $item){
            $arr[] = $item->toArray();
        }
        return $arr;
    }
}
