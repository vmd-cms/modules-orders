<?php

namespace VmdCms\Modules\Orders\Initializers;;

use VmdCms\CoreCms\CoreModules\Events\DTO\CoreEventModelDTO;
use VmdCms\CoreCms\CoreModules\Events\Entity\CoreEventsSetup;
use VmdCms\CoreCms\CoreModules\Events\Enums\CoreEventTypeEnums;
use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;
use VmdCms\Modules\Orders\Services\CoreEventEnums;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'orders';
    const ALIAS = 'Orders';

    public function __construct()
    {
        parent::__construct();
        $this->stubBuilder->setPublishServices(true);
        $this->stubBuilder->setPublishDTO(true);
        $this->stubBuilder->setPublishEntity(true);
    }

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }

    public function seedCoreEvents()
    {
        CoreEventsSetup::getInstance()
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_ORDER_QUICK_CREATE))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_ORDER_CREATE))
                ->setType(CoreEventTypeEnums::NOTIFICATION))
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_BASKET_ACTION))
                ->setType(CoreEventTypeEnums::SYSTEM))
            ->appendEventDTO((new CoreEventModelDTO(CoreEventEnums::USER_CHECKOUT_ACTION))
                ->setType(CoreEventTypeEnums::SYSTEM))
            ->seed();
    }
}
