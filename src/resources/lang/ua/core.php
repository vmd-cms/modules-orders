<?php

return [
    /**
     * fields
     **/
    'sum_orders' => 'Сумма заказов',
    'quantity_orders' => 'Кол-во заказов',
    'statuses_orders' => 'Статусы заказов',
    'all_statuses' => 'Все статусы',
    'quick_buy_orders' => 'Быстрые заказы',

    /**
     * pages
     */


    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
