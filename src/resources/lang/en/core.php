<?php

return [
    /**
     * fields
     **/
    'sum_orders' => 'Sum orders',
    'quantity_orders' => 'Quantity',
    'statuses_orders' => 'Statuses',
    'all_statuses' => 'All statuses',
    'quick_buy_orders' => 'Quick orders',

    /**
     * pages
     */

    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
