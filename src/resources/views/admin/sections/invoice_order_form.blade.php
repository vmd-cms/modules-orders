@if(isset($invoiceOrderDTO) && $invoiceOrderDTO instanceof \VmdCms\Modules\Orders\DTO\Invoice\invoiceOrderDTO)

    @php
        $orderItems = $invoiceOrderDTO->getOrderItems()->getItems();
    @endphp
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table class="header">
                        <tr>
                            <td class="title">
                                <img src="{{$invoiceOrderDTO->getLogoBase64()}}" style="width: 100%; max-width: 300px" />
                            </td>

                            <td class="fs-14px lh-12px">
                                Дата : <input class="custom w-100px" type="text" placeholder="дата" name="date" value="{{$invoiceOrderDTO->getDate()}}"><br />
                                Заказ №: <input class="custom fs-20px w-100px" placeholder="номер заказа" type="text" name="order_number" value="{{$invoiceOrderDTO->getOrderNumber()}}"><br />
                                От: <input class="custom w-100px" type="text" placeholder="дата заказа" name="order_date" value="{{$invoiceOrderDTO->getOrderDate()}}"><br />
                                Дата доставки: <input class="custom w-100px" type="text" placeholder="дата доставки" name="order_delivery_date" value="{{$invoiceOrderDTO->getOrderDeliveryDate()}}">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td class="lh-12px">
                    <div><span class="fs-12px">Оптовый интернет магазин мужской, женской и детской обуви</span></div>
                    <div><span class="fs-12px">+38 050 78-77-002</span></div>
                    <div><span class="fs-12px">www.fartum.ua</span></div>
                    <div><span class="fs-10px">Украина,г.Одесса</span></div>
                </td>
            </tr>

            <tr><td></td></tr>

            <tr class="heading">
                <td colspan="2"><span class="fs-14px">Покупатель</span></td>
            </tr>

            <tr>
                <td class="fs-12px lh-12px">
                    <div><input class="custom w-250px" type="text" placeholder="имя покупателя" name="order_customer_name" value="{{$invoiceOrderDTO->getOrderCustomerName()}}"></div>
                    <div><input class="custom w-250px" type="text" placeholder="телефон получателя" name="order_customer_phone" value="{{$invoiceOrderDTO->getOrderCustomerPhone()}}"></div>
                </td>
            </tr>

            <tr><td></td></tr>

            <tr class="heading">
                <td colspan="2"><span class="fs-14px">Получатель</span></td>
            </tr>

            <tr>
                <td class="fs-12px lh-12px">
                    <div><textarea class="custom w-250px" type="text" placeholder="адрес" name="order_delivery_address">{{$invoiceOrderDTO->getOrderDeliveryAddress()}}</textarea></div>
                    <div><input class="custom w-250px" type="text" placeholder="имя получателя" name="order_delivery_contact_name" value="{{$invoiceOrderDTO->getOrderDeliveryContactName()}}"></div>
                    <div><input class="custom w-250px" type="text" placeholder="телефон получателя" name="order_delivery_contact_phone" value="{{$invoiceOrderDTO->getOrderDeliveryContactPhone()}}"></div>
                </td>
            </tr>

            <tr><td></td></tr>

            <tr>
                <td colspan="2" class="p-unset">
                    <table>
                        <tr class="heading fs-12px">
                            <td class="text-center w-25px" >#</td>
                            <td class="text-center" >Наименование</td>
                            <td class="text-center w-50px" >шт</td>
                            <td class="text-center  w-75px" >цена</td>
                            <td class="text-center  w-75px" >Всего</td>
                        </tr>
                        @if(is_countable($orderItems))
                            @foreach($orderItems as $item)
                                <tr class="fs-12px">
                                    <td class="text-center w-25px">
                                        <input class="custom w-25px text-center" type="text" placeholder="#" name="order_item_number[]" value="{{$item->getId()}}">
                                    </td>
                                    <td>
                                        <input class="custom w-100pr" type="text" placeholder="наименование" name="order_item_title[]" value="{{$item->getTitle()}}">
                                    </td>
                                    <td class="text-center w-50px">
                                        <input class="custom w-50px text-center" type="text" placeholder="шт" name="order_item_quantity[]" value="{{$item->getQuantity()}}">
                                    </td>
                                    <td class="text-center w-75px">
                                        <input class="custom w-75px text-center" type="text" placeholder="цена" name="order_item_price[]" value="{{$item->getPrice()}}">
                                    </td>
                                    <td class="text-center w-75px">
                                        <input class="custom w-75px text-center" type="text" placeholder="всего" name="order_item_total[]" value="{{$item->getTotal()}}">
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </td>
            </tr>

            <tr class="item">
                <td>
                    <div><span class="fs-12px">Итого:</span></div>
                    <div><span class="fs-12px">Доставка: </span><input class="custom w-250px fs-12px" type="text" placeholder="способ доставки" name="order_delivery_type" value="{{$invoiceOrderDTO->getOrderDeliveryType()}}"></div>
                    <div><span class="fs-12px">Оплата: </span><input class="custom w-250px fs-12px" type="text" placeholder="способ оплаты" name="order_payment_type" value="{{$invoiceOrderDTO->getOrderPaymentType()}}"></div>
                </td>

                <td class="text-right">
                    <input class="custom w-250px fs-12px text-right" type="text" placeholder="сумма" name="order_total_str" value="{{$invoiceOrderDTO->getOrderTotalStr()}}">
                </td>
            </tr>

            <tr class="item">
                <td>
                    <div>Итого:</div>
                </td>

                <td class="text-right">
                    <input class="custom w-250px text-right" type="text" placeholder="сумма" name="order_total" value="{{$invoiceOrderDTO->getOrderTotal()}}">
                </td>
            </tr>

            <tr class="item relative">
                <td>
                    <div class="absolute position-center"><span class="fs-12px">М.П.</span></div>
                    <div class="lh-12px mt-10px"><input class="custom w-250px fs-12px border-bottom" type="text" placeholder="отпуск товара произвел" name="order_responsible_manager" value="{{$invoiceOrderDTO->getOrderResponsibleManager()}}"></div>
                    <div class="lh-12px"><span class="fs-10px">Отпуск товара произвел:</span></div>
                </td>

                <td class="text-right">
                    <div class="lh-12px mt-10px"><input class="custom w-250px text-right fs-12px border-bottom" type="text" placeholder="покупатель" name="order_customer_approve" value="{{$invoiceOrderDTO->getOrderCustomerApprove()}}"></div>
                    <div class="lh-12px"><span class="fs-10px">Товар проверил, претензий к комплектации и внешнему виду не имею</span></div>
                </td>
            </tr>
        </table>
    </div>
    <style>
        body {
            font-family: DejaVu Sans, sans-serif;
        }
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
            font-size: 16px;
            line-height: 24px;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table.header tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .invoice-box.rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .invoice-box.rtl table {
            text-align: right;
        }

        .invoice-box.rtl table tr td:nth-child(2) {
            text-align: left;
        }
        input.custom{
            max-width: 100%;
        }
        textarea.custom{
            max-width: 100%;
            border: unset;
        }
        .border-bottom{
            border-bottom: 1px solid #222;
        }
        .relative{
            position: relative;
        }
        .absolute{
            position: absolute;
        }
        .position-center{
            left: 50%;
            transform: translate(-50%,50%);
        }
        .fs-10px{
            font-size: 10px;
        }
        .fs-12px{
            font-size: 12px;
        }
        .fs-14px{
            font-size: 14px;
        }
        .fs-16px{
            font-size: 16px;
        }
        .fs-18px{
            font-size: 18px;
        }
        .fs-20px{
            font-size: 20px;
        }
        .lh-12px{
            line-height: 12px;
        }
        .w-25px{
            width: 25px;
        }
        .w-50px{
            width: 50px;
        }
        .w-75px{
            width: 75px;
        }
        .w-100px{
            width: 100px;
        }
        .w-150px{
            width: 150px;
        }
        .w-200px{
            width: 200px;
        }
        .w-250px{
            width: 250px;
        }
        .w-100pr{
            width: 100%;
        }
        .mt-10px{
            margin-top: 10px;
        }
        .mt-20px{
            margin-top: 20px;
        }
        .p-unset{
            padding: unset!important;
        }
        .text-center{
            text-align: center;
        }
        .text-right{
            text-align: right;
        }
        .text-left{
            text-align: left;
        }
    </style>


@endif
