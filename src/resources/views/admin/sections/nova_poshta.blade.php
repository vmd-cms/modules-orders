@php
    $orderDataArr = isset($model) ? json_decode($model->order_data,true) : [];
    $deliveryData = $orderDataArr['deliveryData'] ?? [];
    $city = $deliveryData['delivery_city_title'] ?? null;
    $warehouse = $deliveryData['delivery_warehouse_title'] ?? null;
@endphp

<div class="view-component-container">
    <div>
        <span>{{$city}}</span>
        <span>{{$warehouse}}</span>
    </div>
</div>
<style>

</style>
