@php
    $orderHistory = isset($model->history) ? $model->history->sortByDesc('id') : [];
@endphp

<div class="view-component-container">
    @if(is_countable($orderHistory))
        <div class="block-title py-5">История заказа</div>
        @foreach($orderHistory as $item)
            <div class="order-history-block">
                <div class="pb-3">{{date('d.m.Y', strtotime($item->created_at)) . ' в ' . date('H:i:s', strtotime($item->created_at))}}</div>
                <div class="history-info">
                    @if($item->action == \VmdCms\Modules\Orders\Entity\OrderHistoryEntity::ACTION_NOTIFICATION)
                        <span class="icon icon-message"></span>
                    @elseif($item->is_moderator)

                        <img class="photo" src="{{$item->moderator->imagePath}}">
                    @else
                        <img class="photo" src="{{$item->user->imagePath}}">
                    @endif
                    <span class="pl-3">{!! $item->value !!}</span>
                </div>
            </div>
        @endforeach
    @endif
</div>
<style>
    .block-title{
        font-size: 16px;
        font-weight: 500;
        line-height: 18px;
    }
    .order-history-block{
        padding: 10px 0;
    }
    .order-history-block .history-info{
        display: flex;
        justify-content: flex-start;
        align-items: center;
    }
    .order-history-block .history-info *{
        font-size: 12px;
        color:#70829A;
    }
    .order-history-block .icon{
        font-size: 18px;
    }
    .order-history-block .photo{
        width: 24px;
        height: 24px;
        object-fit: cover;
        border-radius: 50%;
    }
</style>
