<?php

use App\Modules\Orders\Services\OrderRouter;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;

return function (){
    Route::group([
        'namespace' => "App\\Modules\\Orders\\Controllers",
        'prefix' => "basket",
    ],function (Router $router){
        $router->post('append', [
            'as'   => OrderRouter::ROUTE_APPEND_TO_BASKET,
            'uses' => 'BasketController@appendToBasket',
        ])->withoutMiddleware([\VmdCms\CoreCms\CoreModules\Content\Middleware\DataShareMiddleware::class]);
        $router->post('change-count', [
            'as'   => OrderRouter::ROUTE_CHANGE_BASKET_ITEM_COUNT,
            'uses' => 'BasketController@changeBasketItemCount',
        ])->withoutMiddleware([\VmdCms\CoreCms\CoreModules\Content\Middleware\DataShareMiddleware::class]);
        $router->delete('remove-item', [
            'as'   => OrderRouter::ROUTE_REMOVE_BASKET_ITEM,
            'uses' => 'BasketController@removeBasketItem',
        ])->withoutMiddleware([\VmdCms\CoreCms\CoreModules\Content\Middleware\DataShareMiddleware::class]);
        $router->get('', [
            'as'   => OrderRouter::ROUTE_BASKET,
            'uses' => 'BasketController@basketPage',
        ]);
    });
    Route::group([
        'namespace' => "App\\Modules\\Orders\\Controllers",
        'prefix' => "order",
    ],function (Router $router){
        $router->post('quick-buy', [
            'as'   => OrderRouter::ROUTE_QUICK_BUY,
            'uses' => 'OrderController@quickBuy',
        ]);
        $router->get('quick-buy',function (){
            return abort(404);
        });
    });
};
