<?php

namespace VmdCms\Modules\Orders\ApiServices\NovaPoshta\DTO;

class AddressDTO
{
    /**
     * @var string
     */
    private $present;

    /**
     * @var int
     */
    private $warehouses;

    /**
     * @var string
     */
    private $mainDescription;

    /**
     * @var string
     */
    private $area;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $settlementTypeCode;

    /**
     * @var string
     */
    private $ref;

    /**
     * @var string
     */
    private $deliveryCity;

    /**
     * @var bool
     */
    private $addressDeliveryAllowed;

    /**
     * @var bool
     */
    private $streetsAvailability;

    /**
     * @var string
     */
    private $parentRegionTypes;

    /**
     * @var string
     */
    private $parentRegionCode;

    /**
     * @var string
     */
    private $regionTypes;

    /**
     * @var string
     */
    private $regionTypesCode;

    public function __construct($responseObj)
    {
        $this->present = $responseObj->Present ?? null;
        $this->warehouses = $responseObj->Warehouses ?? null;
        $this->mainDescription = $responseObj->MainDescription ?? null;
        $this->area = $responseObj->Area ?? null;
        $this->region = $responseObj->Region ?? null;
        $this->settlementTypeCode = $responseObj->SettlementTypeCode ?? null;
        $this->ref = $responseObj->Ref ?? null;
        $this->deliveryCity = $responseObj->DeliveryCity ?? null;
        $this->addressDeliveryAllowed = $responseObj->AddressDeliveryAllowed ?? null;
        $this->streetsAvailability = $responseObj->StreetsAvailability ?? null;
        $this->parentRegionTypes = $responseObj->ParentRegionTypes ?? null;
        $this->parentRegionCode = $responseObj->ParentRegionCode ?? null;
        $this->regionTypes = $responseObj->RegionTypes ?? null;
        $this->regionTypesCode = $responseObj->RegionTypesCode ?? null;
    }

    /**
     * @return string|null
     */
    public function getPresent(): ?string
    {
        return $this->present;
    }

    /**
     * @return int
     */
    public function getWarehouses(): int
    {
        return $this->warehouses;
    }

    /**
     * @return string|null
     */
    public function getMainDescription(): ?string
    {
        return $this->mainDescription;
    }

    /**
     * @return string|null
     */
    public function getArea(): ?string
    {
        return $this->area;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @return string|null
     */
    public function getSettlementTypeCode(): ?string
    {
        return $this->settlementTypeCode;
    }

    /**
     * @return string|null
     */
    public function getRef(): ?string
    {
        return $this->ref;
    }

    /**
     * @return string|null
     */
    public function getDeliveryCity(): ?string
    {
        return $this->deliveryCity;
    }

    /**
     * @return bool
     */
    public function isAddressDeliveryAllowed(): bool
    {
        return $this->addressDeliveryAllowed;
    }

    /**
     * @return bool
     */
    public function isStreetsAvailability(): bool
    {
        return $this->streetsAvailability;
    }

    /**
     * @return string|null
     */
    public function getParentRegionTypes(): ?string
    {
        return $this->parentRegionTypes;
    }

    /**
     * @return string|null
     */
    public function getParentRegionCode(): ?string
    {
        return $this->parentRegionCode;
    }

    /**
     * @return string|null
     */
    public function getRegionTypes(): ?string
    {
        return $this->regionTypes;
    }

    /**
     * @return string|null
     */
    public function getRegionTypesCode(): ?string
    {
        return $this->regionTypesCode;
    }

}
