<?php

namespace VmdCms\Modules\Orders\ApiServices\NovaPoshta\DTO;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;

class AddressDTOCollection extends CoreCollectionAbstract
{
    /**
     * @param AddressDTO $dto
     */
    public function append(AddressDTO $dto)
    {
        $this->collection->add($dto);
    }

}
