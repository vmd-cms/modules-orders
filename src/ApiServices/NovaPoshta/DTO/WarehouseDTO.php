<?php

namespace VmdCms\Modules\Orders\ApiServices\NovaPoshta\DTO;

class WarehouseDTO
{
    /**
     * @var string
     */
    private $siteKey;
    private $description;
    private $descriptionRu;
    private $shortAddress;
    private $shortAddressRu;
    private $phone;
    private $typeOfWarehouse;
    private $ref;
    private $number;
    private $cityRef;
    private $cityDescription;
    private $cityDescriptionRu;
    private $settlementRef;
    private $settlementDescription;
    private $settlementAreaDescription;
    private $settlementRegionsDescription;
    private $settlementTypeDescription;
    private $longitude;
    private $latitude;
    private $postFinance;
    private $bicycleParking;
    private $paymentAccess;
    private $pOSTerminal;
    private $internationalShipping;
    private $selfServiceWorkplacesCount;
    private $totalMaxWeightAllowed;
    private $placeMaxWeightAllowed;
    private $districtCode;
    private $warehouseStatus;
    private $warehouseStatusDate;
    private $categoryOfWarehouse;
    private $direct;

    public function __construct($responseObj)
    {
        $this->siteKey = $responseObj->SiteKey ?? null;
        $this->description = $responseObj->Description ?? null;
        $this->descriptionRu = $responseObj->DescriptionRu ?? null;
        $this->shortAddress = $responseObj->ShortAddress ?? null;
        $this->shortAddressRu = $responseObj->ShortAddressRu ?? null;
        $this->phone = $responseObj->Phone ?? null;
        $this->typeOfWarehouse = $responseObj->TypeOfWarehouse ?? null;
        $this->ref = $responseObj->Ref ?? null;
        $this->number = $responseObj->Number ?? null;
        $this->cityRef = $responseObj->CityRef ?? null;
        $this->cityDescription = $responseObj->CityDescription ?? null;
        $this->cityDescriptionRu = $responseObj->CityDescriptionRu ?? null;
        $this->settlementRef = $responseObj->SettlementRef ?? null;
        $this->settlementDescription = $responseObj->SettlementDescription ?? null;
        $this->settlementAreaDescription = $responseObj->SettlementAreaDescription ?? null;
        $this->settlementRegionsDescription = $responseObj->SettlementRegionsDescription ?? null;
        $this->settlementTypeDescription = $responseObj->SettlementTypeDescription ?? null;
        $this->longitude = $responseObj->Longitude ?? null;
        $this->latitude = $responseObj->Latitude ?? null;
        $this->postFinance = $responseObj->PostFinance ?? null;
        $this->bicycleParking = $responseObj->BicycleParking ?? null;
        $this->paymentAccess = $responseObj->PaymentAccess ?? null;
        $this->pOSTerminal = $responseObj->POSTerminal ?? null;
        $this->internationalShipping = $responseObj->InternationalShipping ?? null;
        $this->selfServiceWorkplacesCount = $responseObj->SelfServiceWorkplacesCount ?? null;
        $this->totalMaxWeightAllowed = $responseObj->TotalMaxWeightAllowed ?? null;
        $this->placeMaxWeightAllowed = $responseObj->PlaceMaxWeightAllowed ?? null;
        $this->districtCode = $responseObj->DistrictCode ?? null;
        $this->warehouseStatus = $responseObj->WarehouseStatus ?? null;
        $this->warehouseStatusDate = $responseObj->WarehouseStatusDate ?? null;
        $this->categoryOfWarehouse = $responseObj->CategoryOfWarehouse ?? null;
        $this->direct = $responseObj->Direct ?? null;
    }

    /**
     * @return string
     */
    public function getSiteKey(): string
    {
        return $this->siteKey;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return null
     */
    public function getDescriptionRu()
    {
        return $this->descriptionRu;
    }

    /**
     * @return null
     */
    public function getShortAddress()
    {
        return $this->shortAddress;
    }

    /**
     * @return null
     */
    public function getShortAddressRu()
    {
        return $this->shortAddressRu;
    }

    /**
     * @return null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return null
     */
    public function getTypeOfWarehouse()
    {
        return $this->typeOfWarehouse;
    }

    /**
     * @return null
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @return null
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return null
     */
    public function getCityRef()
    {
        return $this->cityRef;
    }

    /**
     * @return null
     */
    public function getCityDescription()
    {
        return $this->cityDescription;
    }

    /**
     * @return null
     */
    public function getCityDescriptionRu()
    {
        return $this->cityDescriptionRu;
    }

    /**
     * @return null
     */
    public function getSettlementRef()
    {
        return $this->settlementRef;
    }

    /**
     * @return null
     */
    public function getSettlementDescription()
    {
        return $this->settlementDescription;
    }

    /**
     * @return null
     */
    public function getSettlementAreaDescription()
    {
        return $this->settlementAreaDescription;
    }

    /**
     * @return null
     */
    public function getSettlementRegionsDescription()
    {
        return $this->settlementRegionsDescription;
    }

    /**
     * @return null
     */
    public function getSettlementTypeDescription()
    {
        return $this->settlementTypeDescription;
    }

    /**
     * @return null
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @return null
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return null
     */
    public function getPostFinance()
    {
        return $this->postFinance;
    }

    /**
     * @return null
     */
    public function getBicycleParking()
    {
        return $this->bicycleParking;
    }

    /**
     * @return null
     */
    public function getPaymentAccess()
    {
        return $this->paymentAccess;
    }

    /**
     * @return null
     */
    public function getPOSTerminal()
    {
        return $this->pOSTerminal;
    }

    /**
     * @return null
     */
    public function getInternationalShipping()
    {
        return $this->internationalShipping;
    }

    /**
     * @return null
     */
    public function getSelfServiceWorkplacesCount()
    {
        return $this->selfServiceWorkplacesCount;
    }

    /**
     * @return null
     */
    public function getTotalMaxWeightAllowed()
    {
        return $this->totalMaxWeightAllowed;
    }

    /**
     * @return null
     */
    public function getPlaceMaxWeightAllowed()
    {
        return $this->placeMaxWeightAllowed;
    }

    /**
     * @return null
     */
    public function getDistrictCode()
    {
        return $this->districtCode;
    }

    /**
     * @return null
     */
    public function getWarehouseStatus()
    {
        return $this->warehouseStatus;
    }

    /**
     * @return null
     */
    public function getWarehouseStatusDate()
    {
        return $this->warehouseStatusDate;
    }

    /**
     * @return null
     */
    public function getCategoryOfWarehouse()
    {
        return $this->categoryOfWarehouse;
    }

    /**
     * @return null
     */
    public function getDirect()
    {
        return $this->direct;
    }


}
