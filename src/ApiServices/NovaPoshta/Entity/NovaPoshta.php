<?php

namespace VmdCms\Modules\Orders\ApiServices\NovaPoshta\Entity;

use App\Modules\Content\Entity\Languages;
use VmdCms\Modules\Orders\ApiServices\NovaPoshta\DTO\AddressDTO;
use VmdCms\Modules\Orders\ApiServices\NovaPoshta\DTO\AddressDTOCollection;
use VmdCms\Modules\Orders\ApiServices\NovaPoshta\DTO\WarehouseDTO;
use VmdCms\Modules\Orders\ApiServices\NovaPoshta\DTO\WarehouseDTOCollection;
use VmdCms\Modules\Orders\ApiServices\NovaPoshta\Exceptions\ApiException;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Config;

class NovaPoshta
{
    private $key;
    private $url;
    private $lang;

    public function __construct()
    {
        $this->key = Config::get('services.nova_poshta.key');
        $this->url = 'https://api.novaposhta.ua/v2.0/json/';
        $this->lang = Languages::getInstance()->getCurrentLocaleActive();
    }

    /**
     * @param string $needle
     * @param int $limit
     * @return AddressDTOCollection
     */
    public function getAddresses(string $needle, $limit = 10): AddressDTOCollection
    {
        $collection = new AddressDTOCollection();
        $body = [
            "modelName" =>"Address",
            "calledMethod" => "searchSettlements",
            "methodProperties"=> [
                "CityName"=> $needle,
                "Limit"=> $limit
            ]
        ];

        $result = $this->request($body);
        $data = $result['data'][0] ?? null;
        if(isset($data,$data->Addresses) && is_countable($data->Addresses)){
            foreach ($data->Addresses as $address){
                $collection->append(new AddressDTO($address));
            }
        }
        return $collection;
    }

    /**
     * @param string $cityRef
     * @param int $limit
     * @param int $page
     * @return WarehouseDTOCollection
     * @throws ApiException
     */
    public function getWarehouses(string $cityRef, $limit = 10, $page = 1): WarehouseDTOCollection
    {
        $collection = new WarehouseDTOCollection();

        if(empty($this->key)){
            throw new ApiException('Api key not set');
        }

        $body = [
            "modelName" =>"AddressGeneral",
            "calledMethod" => "getWarehouses",
            "methodProperties"=> [
                "Language" => $this->lang,
                "CityRef" => $cityRef,
                "Limit" => $limit,
                "Page" => $page
            ]
        ];

        $result = $this->request($body);
        $data = $result['data'];
        if(is_countable($data)){
            foreach ($data as $item){
                $collection->append(new WarehouseDTO($item));
            }
        }
        return $collection;
    }

    private function request($options = [])
    {
        try {
            $client = new Client();
            $request = $client->post($this->url, [
                RequestOptions::JSON => $options
            ]);
            $response = json_decode($request->getBody()->getContents());
            $success = $response ? $response->success : false;
            $data = $response ? $response->data : [];
        }
        catch (\Exception $exception){
            $success = false;
            $data = null;
        }
        return [
            'success' => $success,
            'data' => $data
        ];
    }
}
